PicketLink Vault extension for Apache Tomcat.

See the LICENSE file distributed with this work for information
regarding licensing.

=====================================================================

Requirements:
-------------

* Tomcat Vault release tarball or source repository
* Apache Tomcat

Retrieving the Vault:
---------------------
Download the Vault tarball for applicable Tomcat branch:
* Latest Release - [1.2.0 RC1](https://gitlab.com/danluca/tomcat-vault/-/releases/1.2.0-rc1)
    * [Vault for Tomcat 8.5](https://gitlab.com/danluca/tomcat-vault/-/jobs/532187241/artifacts/raw/build/distributions/tomcat-vault-1.2.0.rc1-tc8.5.54.tar.gz)
    * [Vault for Tomcat 9.0](https://gitlab.com/danluca/tomcat-vault/-/jobs/532187242/artifacts/raw/build/distributions/tomcat-vault-1.2.0.rc1-tc9.0.34.tar.gz)
* Latest Build Artifacts
    * [Vault for Tomcat 8.5](https://gitlab.com/danluca/tomcat-vault/-/jobs/artifacts/master/download?job=assemble85)
    * [Vault for Tomcat 9.0](https://gitlab.com/danluca/tomcat-vault/-/jobs/artifacts/master/download?job=assemble90)


Building the Vault from Source:
---------------------------------
Download the source - either from latest [release](https://gitlab.com/danluca/tomcat-vault/-/archive/1.2.0-rc1/tomcat-vault-1.2.0-rc1.zip) 
or [repository](https://gitlab.com/danluca/tomcat-vault/-/archive/master/tomcat-vault-master.zip).

It is recommended that the vault is built on a different system than the Tomcat host (due to amount of file downloads during build). 
In the commands examples below, it is assumed that this machine is a Windows OS. If on *nix OS, please adjust accordingly the main command names. 
The repository contains proper files for both OS.

1. Update `gradle.properties` for the target Tomcat version - the file defaults to Tomcat 8.5.54. Both 8.5 and 9.0 Tomcat branches are supported.
2. Ensure `JAVA_HOME` points to a JDK of the same API level Tomcat is targeted to run with. 
**Note:** JDK lower than 8 are not supported.
3. Assemble Tomcat Vault from its source
    ~~~
    # this command uses the gradle wrapper, it will download gradle locally
    ./gradlew.bat distro
    ~~~

Copy Artifacts to Tomcat Host:
-----------------------------
1. Copy the distribution tarball from `./build/distributions/*.tgz` to the Tomcat host machine using your preferred method
2. Create a keystore with two **AES 256bit** symmetric keys - use your favorite method (CLI `keytool`, [Keystore Explorer](https://keystore-explorer.org/), etc)
    * the supported keystore types are PKCS12 (preferred, portable) and JCEKS (not very portable between JVM vendors)
    * ensure the keys are protected by the same password as the keystore itself
    * make a note of the two keys aliases. Allocate one for encrypting vault stored attributes and the other for encrypting attributes not stored in the vault - aka [inline protection](#inline-password-protection).
3. Copy the keystore onto the Tomcat host

Deploying and Setting up the Vault:
-------------------
The system that hosts Tomcat is assumed running a *nix OS.

1. Install Apache Tomcat - if not already (from an RPM, by hand, or however you prefer)
2. Configure in the shell terminal the path to Tomcat installation as `CATALINA_BASE` local variable.
3. Uncompress the `tgz` vault file copied
4. Copy the contents of `lib` directory to `$CATALINA_BASE/lib/`:

    ~~~
    $ cp lib/* $CATALINA_BASE/lib/
    ~~~

5. Copy the contents of `bin` directory to `$CATALINA_BASE/bin/`:

    ~~~
    $ cp bin/* $CATALINA_BASE/bin/
    ~~~
   
6. Add the following line to `$CATALINA_BASE/conf/catalina.properties` so that Tomcat's Digester uses the tomcat-vault PropertySource implementation:

    ~~~
    org.apache.tomcat.util.digester.PROPERTY_SOURCE=org.apache.tomcat.vault.util.PropertySourceVault
    ~~~

7. Setup your Vault. Here is an example (keystore file name is `vault.p12` below):
    ~~~
    # run vault with help option to familiarize with arguments
    $ ./bin/vault.sh -h
    ~~~

    ~~~
    # Make a directory for the Vault to live
    $ mkdir $CATALINA_BASE/conf/vault

    # Copy the keystore for the Vault
    $ cp vault.p12 $CATALINA_BASE/conf/vault/

    # Initialize the Vault and save vault.properties
    # short form of the arguments: -k -> --keystore, -Av -> --alias-vault, -Ai -> --alias-inline
    # -i -> --iteration, -s -> --salt, -e -> --enc-dir, -g -> --generate-config
    $ cd $CATALINA_BASE
    $ ./bin/vault.sh -k ./conf/vault/vault.p12 -Av vault_key -Ai inline_key -e ./conf/vault/ -i 181 -s 1234abcdX%$#Z -g $CATALINA_BASE/conf/vault.properties
    ~~~

    **Note:** You can also initialize the Vault in an interactive mode by executing bin/vault.sh with no arguments. If you do this, then you will need to create a file named vault.properties in `$CATALINA_BASE/conf` containing your Vault information as below (all of these keys must be defined and **NOT** empty). This information is provided by the interactive session at the end of the initialization.

    ~~~
    KEYSTORE_URL=..
    KEYSTORE_PASSWORD=..
    KEYSTORE_TYPE=..
    KEY_SIZE=256
    VAULT_ALIAS=..
    INLINE_ALIAS=..
    SALT=..
    ITERATION_COUNT=..
    ENC_FILE_DIR=..
    ~~~

8. Add passwords to the vault - for instance:

    ~~~
    # These commands will ask for the passwor at the console. If comfortable, the password can also be passed as an argument after -x parameter
    $ ./bin/vault.sh -f ./conf/vault.properties -b oracle -a device.password -x
   ...
    $ ./bin/vault.sh -f ./conf/vault.properties -b oracle -a presentation.password -x
    ...
   
    # confirm the vault contains the entries
    $ ./bin/vault.sh -f ./conf/vault.properties -l
    ~~~

9. Update the Tomcat XML files to use the passwords from the vault using the syntax `${#vault[<block_name>:<attribute_name>]}` - example:
    ~~~
    ${#vault[oracle:device.password]}
    ~~~

    * for instance - in the case of `tomcat-users.xml` file - change the user in tomcat-users.xml from:
    
        ~~~
        <user username="tomcat" password="P@SSW0#D" roles="manager-gui"/>
        ~~~
    
        to:
    
        ~~~
        <user username="tomcat" password="${#vault[my_block:manager_password]}" roles="manager-gui"/>
        ~~~

10. Start Apache Tomcat! (there shall be no errors in the startup tomcat log)

### Host file permissions

All vault related assets have at least two layers of protection in addition to the intrinsic filesystem access control permissions:
* the keystore file is password protected, the password is encrypted in vault's config file
    * keystore's password is encrypted with a key derived from three elements:
        * a pre-defined scrambled password
        * a configured salt 
        * a configured number of iterations
    * the configured items - *salt* and number of *iterations* - are specified in the vault properties file.
* the passwords are stored encrypted in the vault file, the keys are in the keystore
* the [inline](#inline-password-protection) protected passwords are encrypted with a key protected by the keystore
* the `vault.properties` file contains the keystore's encrypted password, and the configuration needed to derive the password's encryption key. 
Therefore, its access control permissions should be reviewed with more scrutiny. 

On this basis, the file permissions for config and vault files on Tomcat host can be set as follows, for example:
* **600** (owner: r/w; group, others: none)
    * vault properties - `conf/vault.properties`. Can be promoted to `640` (add read only to group) if the group is sufficiently restrictive
* **644** (owner: r/w; group, others: read only) - or the standard tomcat config file permissions in your configuration
    * the keystore - e.g. `conf/vault/vault.p12`
    * the vault - e.g. `conf/vault/VAULT.dat`
    * the vault folder - e.g. `conf/vault`
    * `catalina.properties`
 


Inline password protection
--------------------------
As we have seen above, in the Tomcat XML files we refer to a password **stored** in the vault through its *handle* as `#vault[<block_name>:<attribute_name>]`
with both `<block_name>` and `<attribute_name>` being required. 

There is also an option to protect a password **without** having to store it in the vault - we call this *inline* protection.
Here's an example:
~~~
# generate the value of the protected password for inline usage
$ ./bin/vault.sh -f ./conf/vault.properties -E
...
>INFO: Default Security Vault Implementation Initialized and Ready
>Enter password to encrypt (inline) :
>Enter password to encrypt (inline) again:
>        Values match
>Encrypted value: #vault[cCq8L93qTH0BgTK95oePIrE3RoMDtbzWgM5ez+//WzEBtdNH]
>Ensure to set property INLINE_ALIAS=inline_key in vault properties file
~~~

In the Tomcat XML file then use this for the value of the protected password: `${#vault[cCq8L93qTH0BgTK95oePIrE3RoMDtbzWgM5ez+//WzEBtdNH]}`:
~~~
<user username="tomcat" password="${#vault[cCq8L93qTH0BgTK95oePIrE3RoMDtbzWgM5ez+//WzEBtdNH]}" roles="manager-gui"/>
~~~

Redirected property resolution
------------------------------

If desired (note that redirected property resolution is slightly less efficient), we can have a setup where the Tomcat XML file refers to a property 
defined in `catalina.properties` with a vault protected value (by handle or inline) - example:
~~~
# catalina.properties
...
org.apache.tomcat.util.digester.PROPERTY_SOURCE=org.apache.tomcat.vault.util.PropertySourceVault
...
manager.password=#vault[my_block:manager_password]
~~~
~~~
<!-- XML config file ->
<user username="tomcat" password="${manager.password}" roles="manager-gui"/>
~~~

Efficiency aside, this allows for protected attributes (passwords, etc) to be supplied or overridden through **environment variables** (as the environment variable values take precedence over other property values defined).
Hint, hint Docker and Kubernetes :wink:

Note that property names using dot notation map to environment variable names by replacing `.` with `_` and converting to UPPERCASE.
In the example above, we can have an environment variable `MANAGER_PASSWORD` with a value of inline or vault stored protected password:
~~~
$ export MANAGER_PASSWORD='#vault[cCq8L93qTH0BgTK95oePIrE3RoMDtbzWgM5ez+//WzEBtdNH]'
~~~ 