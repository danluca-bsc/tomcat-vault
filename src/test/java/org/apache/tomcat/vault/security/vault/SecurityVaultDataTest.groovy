/*
 * Copyright 2020 Tomcat-Vault contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.tomcat.vault.security.vault

import spock.lang.Specification

/**
 * Unit test for {@link SecurityVaultData}
 * @author Dan Luca
 * @trace org.apache.tomcat.vault.security.vault.SecurityVaultData
 */
class SecurityVaultDataTest extends Specification {
    SecurityVaultData vaultData
    URL vaultUrl

    void setup() {
        vaultData = new SecurityVaultData()
        vaultUrl = SecurityVaultDataTest.class.getResource("conf/VAULT.dat")
    }

    def "GetVaultData"() {
        given:
            String keyType = "don't matter"
            String vaultBlock = "data_block"
            String attribute = "manager_password"
        and:
            new File(vaultUrl.toURI()).withObjectInputStream {
                vaultData = it.readObject()
            }
        when:
            byte[] data = vaultData.getVaultData(keyType, vaultBlock, attribute)
        then:
            assert data
        when:
            vaultBlock="other.block"
            data = vaultData.getVaultData(keyType, vaultBlock, attribute)
        then:
            assert data == null
    }

    def "ContainsData"() {
        given:
            String keyType = "don't matter"
            String vaultBlock = "data_block"
            String attribute = "manager_password"
        and:
            new File(vaultUrl.toURI()).withObjectInputStream {
                vaultData = it.readObject()
            }
        when:
            boolean contains = vaultData.containsData(keyType, vaultBlock, attribute)
        then:
            assert contains
        when:
            vaultBlock="other.block"
            contains = vaultData.getVaultData(keyType, vaultBlock, attribute)
        then:
            assert !contains
    }

    def "AddVaultData"() {
        given:
            String keyType = "don't matter"
            String vaultBlock = "data_block"
            String attribute = "manager_password"
            String password = "3r489ydfuhvjcnxrml34897t%%^"
        when:
            vaultData.addVaultData(keyType, vaultBlock, attribute, password.bytes)
        then:
            assert new String(vaultData.getVaultData(null, vaultBlock, attribute)) == password
    }

    def "DeleteVaultData"() {
        given:
            String keyType = "don't matter"
            String vaultBlock = "data_block"
            String attribute = "manager_password"
            String password = "3r4i87634ryfd vhcnx aksfhnc"
        and:
            assert vaultData.vaultDataKeys.isEmpty()
            vaultData.addVaultData(keyType, vaultBlock, attribute, password.bytes)
        when:
            boolean done = vaultData.deleteVaultData(null, vaultBlock, attribute)
        then:
            assert done
            assert vaultData.vaultDataKeys.isEmpty()
    }

    def "GetVaultDataKeys"() {
        given:
            new File(vaultUrl.toURI()).withObjectInputStream {
                vaultData = it.readObject()
            }
        when:
            Set<String> vaultEntryNames = vaultData.getVaultDataKeys()
        then:
            assert vaultEntryNames
            assert vaultEntryNames.size() == 2
            assert vaultEntryNames.find { it == "data_block:manager_password"}
            assert vaultEntryNames.find { it == "oracle:device_password"}
    }
}
