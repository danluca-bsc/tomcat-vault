/*
 * Copyright 2020 Tomcat-Vault contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.tomcat.vault.security

import org.apache.tomcat.vault.VaultTool
import spock.lang.Specification
import spock.lang.Unroll

import java.nio.file.Paths

/**
 * Unit test for {@link org.apache.tomcat.vault.security.Util}
 * @author danluca
 */
class UtilTest extends Specification {

    boolean windows = false
    def filePath
    void setup() {
        filePath = Paths.get(UtilTest.class.getResource("secure.txt").toURI()).toAbsolutePath().toString()
        windows = VaultTool.isWindows()
    }

    def "LoadPassword"() {
        given:
            def pswCmd = windows ? "#cmd{cmd.exe /c type $filePath}" : "#cmd{cat $filePath}"
        when:
            def loadedPsw = Util.loadPassword(pswCmd)
        then:
            assert loadedPsw == new File(filePath).readLines()[0]
        when:
            def noCmdPsw = $/s1mplePa$$word/$
            loadedPsw = Util.loadPassword(noCmdPsw)
        then:
            assert noCmdPsw == loadedPsw
    }

    @Unroll("Is #psw a command - #isCommand")
    def "IsPasswordCommand"() {
        expect:
            assert Util.isPasswordCommand(psw) == isCommand
        where:
            psw                         | isCommand
            "#cmd{}"                    | true
            "#cmd:21600{}"              | true
            "#cmd:3600{cat secure.txt}" | true
            "cmd:3600{}"                | false
            "another_password"          | false
    }

    def "SafeClose-InputStream"() {
        given:
            def mockStream = Mock(InputStream)
        when:
            Util.safeClose(mockStream)
        then:
            noExceptionThrown()
        when:
            Util.safeClose(mockStream, "Closing Input Stream did not work")
        then:
            noExceptionThrown()
        when:
            Util.safeClose(mockStream)
        then:
            interaction {
                _ * mockStream.close() >> {throw new RuntimeException("waaah!")}
            }
        noExceptionThrown()
    }

    def "SafeClose - Output"() {
        given:
            def mockStream = Mock(OutputStream)
        when:
            Util.safeClose(mockStream)
        then:
            noExceptionThrown()
        when:
            Util.safeClose(mockStream, "Closing Input Stream did not work")
        then:
            noExceptionThrown()
        when:
            Util.safeClose(mockStream)
        then:
            interaction {
                _ * mockStream.close() >> {throw new RuntimeException("waaah!")}
            }
            noExceptionThrown()
    }
}
