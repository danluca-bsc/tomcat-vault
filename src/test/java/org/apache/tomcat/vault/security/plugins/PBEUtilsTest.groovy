/*
 * Copyright 2020 Tomcat-Vault contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.tomcat.vault.security.plugins

import org.apache.tomcat.vault.security.vault.SecurityVault
import spock.lang.Specification

import javax.crypto.SecretKey
import java.security.GeneralSecurityException
import java.util.concurrent.ThreadLocalRandom

/**
 * Unit test for {@link PBEUtils}
 * @author danluca
 * @trace org.apache.tomcat.vault.security.plugins.PBEUtils
 */
class PBEUtilsTest extends Specification {
    PBEUtils pbeUtils

    void setup() {
        Random r = ThreadLocalRandom.current();
        StringBuilder sbSalt = new StringBuilder()
        (0..40).each {sbSalt.append((char)r.nextInt(0x20, 0x7E))}
        String salt = sbSalt.toString()
        int iter = r.nextInt(20, 1024)
        println "Salt: ${salt}; iterations: $iter"
        pbeUtils = new PBEUtils(salt, iter)
    }

    def "EncodeDecode"() {
        given:
            def str = "stuff I want protected with default encryption"
        when:
            def enc = pbeUtils.encode(str)
            enc = enc.replace(SecurityVault.MASK_PROPERTY_PLACEHOLDER_PREFIX, SecurityVault.MASK_PROPERTY_PLACEHOLDER_PREFIX.toUpperCase())
            println "Encrypted: $enc"
            def dec = pbeUtils.decode(enc)
        then:
            println "Decrypted: $dec"
            assert dec == str
    }

    def "DecodeNonMasked"() {
        given:
            def str = "stuff I want protected with default encryption"
        when:
            def enc = pbeUtils.encode(str)
            println "Encrypted: $enc"
            def dec = pbeUtils.decode(enc.substring(1))
        then:
            assert dec != str
            assert dec == enc.substring(1)
    }

    def "DecodeFailure"() {
        given:
            def str = "stuff I want"
            String enc = pbeUtils.encode(str)

            Closure<String> modify = { String b64->
                byte[] arr = b64.decodeBase64()
                arr[-1] += 1
                arr.encodeBase64().toString()
            }

            println "Original Encrypted: $enc"
            def b64Part = enc.substring(SecurityVault.MASK_PROPERTY_PLACEHOLDER_PREFIX.length(), enc.length()-1)
            enc = "${SecurityVault.MASK_PROPERTY_PLACEHOLDER_PREFIX}${modify(b64Part)}${SecurityVault.DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX}"
            println "Modified Encrypted: $enc"
        when:
            def dec = pbeUtils.decode(enc)
        then:
            thrown(GeneralSecurityException)
    }

    def "GenerateKey"() {
        when:
            SecretKey sk = PBEUtils.generateKey("random text as password", pbeUtils.salt, pbeUtils.iterationCount)
        then:
            assert sk.algorithm == "AES"
            assert sk.encoded.length == 256/8
    }
}
