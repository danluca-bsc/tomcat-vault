/*
 * Copyright 2020 Tomcat-Vault contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.tomcat.vault.security.vault

import spock.lang.Specification

import java.nio.file.Paths

/**
 * Unit test for {@link PicketBoxSecurityVault}
 * @author danluca
 * @trace org.apache.tomcat.vault.security.vault.PicketBoxSecurityVault
 */
class PicketBoxSecurityVaultTest extends Specification {
    PicketBoxSecurityVault securityVault
    Map<String, String> options = [:]

    void setup() {
        securityVault = new PicketBoxSecurityVault()
        URL cfgUrl = PicketBoxSecurityVaultTest.class.getResource("conf/vault.properties")
        String encDir = Paths.get(cfgUrl.toURI()).parent.normalize().toString()
        String keystoreLoc = Paths.get(encDir).resolve("vault.p12").normalize().toString()
        Properties cfg = new Properties()
        cfgUrl.withInputStream {cfg.load(it)}

        cfg.setProperty("ENC_FILE_DIR", encDir)
        cfg.setProperty("KEYSTORE_URL", keystoreLoc)

        options.putAll(cfg)
    }

    def "Init"() {
        when:
            securityVault.init(options)
        then:
            assert securityVault.vaultContent
            assert securityVault.pbe
    }

    def "IsInitialized"() {
        given:
            securityVault.init(options)
        when:
            def initialized = securityVault.isInitialized()
        then:
            assert initialized
    }

    def "KeyList"() {
        given:
            securityVault.init(options)
        when:
            def keys = securityVault.keyList()
        then:
            assert keys
            assert keys.find {it == "data_block:manager_password"}
            assert keys.find {it == "oracle:device_password"}
            assert keys.size() == 2
    }

    def "Store"() {
        given:
            securityVault.init(options)
        when:
            securityVault.store("test", "attribute.name", "blah-blah")
            def keys = securityVault.keyList()
        then:
            assert keys.size() == 3
            assert keys.find {it == "test:attribute.name"}
    }

    def "Retrieve"() {
        given:
            securityVault.init(options)
        when:
            def value = securityVault.retrieve("test", "attribute.name")
        then:
            assert value == "blah-blah"
        when:
            def nullValue = securityVault.retrieve("test", "attribute_name")
        then:
            thrown(SecurityVaultException)
    }

    def "Exists"() {
        given:
            securityVault.init(options)
        when:
            def exist = securityVault.exists("test", "attribute.name")
            def notExist = securityVault.exists("test", "attribute_name")
        then:
            assert exist
            assert !notExist
    }

    def "Remove"() {
        given:
            securityVault.init(options)
        when:
            def removed = securityVault.remove("test", "attribute.name")
            def notRemoved = securityVault.exists("test", "attribute_name")
        then:
            assert removed
            assert !notRemoved

    }
}
