/*
 * Copyright 2020 Tomcat-Vault contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.tomcat.vault.util

import spock.lang.Specification

import java.nio.file.Paths

/**
 * Unit test for {@link PropertySourceVault}
 * @author Dan Luca
 * @trace org.apache.tomcat.vault.util.PropertySourceVault
 */
class PropertySourceVaultTest extends Specification {
    PropertySourceVault propSource

    void setup() {
        URL vault = PropertySourceVaultTest.class.getResource("conf/vault.properties")
        String tomcatHome = Paths.get(vault.toURI()).parent.parent.normalize().toString()
        String keystoreLoc = Paths.get(tomcatHome).resolve("conf/vault.p12").normalize().toString()
        String encDir = Paths.get(tomcatHome).resolve("conf").normalize().toString()

        System.properties.setProperty("catalina.base", tomcatHome)

        Properties cfg = new Properties()
        vault.withInputStream {cfg.load(it)}
        cfg.setProperty("ENC_FILE_DIR", encDir)
        cfg.setProperty("KEYSTORE_URL", keystoreLoc)
        Paths.get(vault.toURI()).toFile().withOutputStream {cfg.store(it, null)}
    }

    def "Init"() {
        when:
            propSource = new PropertySourceVault()
        then:
            noExceptionThrown()
    }

    def "GetProperty"() {
        given:
            propSource = new PropertySourceVault()
        when: "resolve straight from vault"
            String mgrPassword = propSource.getProperty("#vault[oracle:device_password]")
        then:
            assert mgrPassword == "oracle"
        when: "delegate to catalina.properties and resolve from vault"
            mgrPassword = propSource.getProperty("application.database.password")
        then:
            assert mgrPassword == "oracle"
        when: "non existing property"
            mgrPassword = propSource.getProperty("umpa.lumpa")
        then:
            assert mgrPassword == "umpa.lumpa"
        when: "non existing property with default value specified"
            mgrPassword = propSource.getProperty("umpa.lumpa:ChocoLate")
        then:
            assert mgrPassword == "ChocoLate"
        when: "empty input"
            mgrPassword = propSource.getProperty("")
        then:
            assert mgrPassword == ""
        when: "compounded property"
            mgrPassword = propSource.getProperty("application.compounded.property.dont.do.this")
        then:
            assert mgrPassword == "oracle with tzatza"
        when: "property overridden as system property"
            System.setProperty("application.database.password", "#vault[data_block:manager_password]")
            mgrPassword = propSource.getProperty("application.database.password")
        then:
            assert mgrPassword == "tzatza"
        when: "property overridden as environment property"
            mgrPassword = propSource.getProperty("path")
        then:
            assert mgrPassword != "path"
        when: "inline protection"
            mgrPassword = propSource.getProperty("#vault[ARPCN/RU11Dy089e6GV+34ma/pwXCcFTOjfvP34obLaObcKu6SA=]")
        then:
            assert mgrPassword == "mySql2020!"
        when: "property not in vault"
            String unstoredProperty = "#vault[fancy_block:undefined.attribute]"
            mgrPassword = propSource.getProperty(unstoredProperty)
        then:
            assert mgrPassword == unstoredProperty
    }
}
