/*
 * Copyright 2020 Tomcat-Vault contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.tomcat.vault.util

import org.apache.tomcat.vault.security.plugins.PBEUtils
import spock.lang.Specification

import javax.crypto.SecretKey
import java.security.GeneralSecurityException

/**
 * Unit test for {@link EncryptionUtil}
 * @author danluca
 * @trace org.apache.tomcat.vault.util.EncryptionUtil
 */
class EncryptionUtilTest extends Specification {

    void setup() {
    }

    def "EncDecrypt-B64"() {
        given:
            def str = "I would like to see myself encrypted with a good and secure algorithm"
            SecretKey key = PBEUtils.generateKey('my big pa$sw0r2<>', "laskHFIQwrsdn m,dAK?DOi3'0", 193)
        when:
            def encStr = EncryptionUtil.encryptB64(str, key)
            def clearStr = EncryptionUtil.decryptB64(encStr, key)
        then:
            assert clearStr == str
    }

    def "EncDecrypt"() {
        given:
            SecretKey key = PBEUtils.generateKey("pass", "word", 854)
            def str = "I'll tell you my best password ever that I don't use to every site"
            def bytes = str.getBytes()
        when:
            byte[] encBytes = EncryptionUtil.encrypt(bytes, key)
            byte[] decBytes = EncryptionUtil.decrypt(encBytes, key)
        then:
            assert new String(decBytes) == str
    }

    def "DecryptFailure"() {
        given:
            SecretKey key = PBEUtils.generateKey("my long password that haunts me", "salt or hot pepper", 653)
            def str = "short password that I definitely did not share"
            def enc = EncryptionUtil.encryptB64(str, key)
            byte[] encArr = enc.decodeBase64()
            byte[] modEnc =Arrays.copyOf(encArr, encArr.length + 1)
            modEnc[-1] = 0x20
        when:
            def clear = EncryptionUtil.decryptB64(modEnc.encodeBase64().toString(), key)
        then:
            thrown(GeneralSecurityException)
    }
}
