/*
 * Copyright 2020 Tomcat-Vault contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.tomcat.vault.util

import spock.lang.Specification

/**
 * Unit test for {@link PropertyFileManager}
 * @author Dan Luca
 * @trace org.apache.tomcat.vault.util.PropertyFileManager
 */
class PropertyFileManagerTest extends Specification {
    PropertyFileManager pfm
    File tempFile
    Properties props
    void setup() {
        tempFile = File.createTempFile("tomcat-vault", PropertyFileManagerTest.class.simpleName)
        tempFile.deleteOnExit()
        props = new Properties()
        props.setProperty("property.1", "value one")
        props.setProperty("property.second", "value two")
        pfm = new PropertyFileManager(tempFile.absolutePath)
    }

    def "Save"() {
        given:
            Properties readProps = new Properties()
        when:
            pfm.save(props)
            tempFile.withInputStream { readProps.load(it) }
        then:
            assert readProps == props
    }

    def "Load"() {
        given:
            pfm.save(props)
        when:
            Properties readProps = pfm.load()
        then:
            assert readProps == props
    }
}
