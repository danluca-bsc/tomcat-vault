/*
 * Copyright 2020 Tomcat-Vault contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.tomcat.vault.util

import spock.lang.Specification

/**
 * Unit test for {@link StringUtil}
 * @author Dan Luca
 * @trace org.apache.tomcat.vault.util.StringUtil
 */
class StringUtilTest extends Specification {
    void setup() {
    }

    def "IsNotNull"() {
        expect:
            assert !StringUtil.isNotEmpty("")
            assert StringUtil.isNotEmpty(" ")
            assert StringUtil.isNotEmpty("blah")
            assert !StringUtil.isNotEmpty(null)
    }

    def "GetSystemPropertyAsString"() {
        given:
            String propName = "blah.blah.bla"
            String decPropName = "\${$propName}"
        when:
            def prop = StringUtil.getSystemPropertyAsString(decPropName)
        then:
            assert prop == decPropName
    }

    def "Mix"() {
        given:
            String one = "1234567"
            String two = "abcdefghijk"
            String three = "!@#\$%"
        when:
            String mixed = StringUtil.mix(one, two, three)
        then:
            assert mixed == "1a!2b@3c#4d\$5e%6f7ghijk"
    }

    def "SubstringAfter"() {
        expect:
            assert "barXXbaz" == StringUtil.substringAfter("fooXXbarXXbaz", "XX")
            assert StringUtil.substringAfter(null, null) == null
            assert StringUtil.substringAfter(null, "") == null
            assert StringUtil.substringAfter(null, "XX") == null
            assert "" == StringUtil.substringAfter("", null)
            assert "" == StringUtil.substringAfter("", "")
            assert "" == StringUtil.substringAfter("", "XX")

            assert "" == StringUtil.substringAfter("foo", null)
            assert "ot" == StringUtil.substringAfter("foot", "o")
            assert "bc" == StringUtil.substringAfter("abc", "a")
            assert "cba" == StringUtil.substringAfter("abcba", "b")
            assert "" == StringUtil.substringAfter("abc", "c")
            assert "abc" == StringUtil.substringAfter("abc", "")
            assert "" == StringUtil.substringAfter("abc", "d")
    }

    def "IsEmpty"() {
        expect:
            assert StringUtil.isEmpty(null)
            assert StringUtil.isEmpty("")
            assert !StringUtil.isEmpty(" ")
            assert !StringUtil.isEmpty("blah")
    }

    def "StartsWith"() {
        given:
            String foo    = "foo";
            String bar    = "bar";
            String foobar = "foobar";
            String FOO    = "FOO";
            String BAR    = "BAR";
            String FOOBAR = "FOOBAR";
        expect:
            assert StringUtil.startsWith(null, null), "startsWith(null, null)"
            assert !StringUtil.startsWith(FOOBAR, null), "startsWith(FOOBAR, null)"
            assert !StringUtil.startsWith(null, FOO), "startsWith(null, FOO)"
            assert StringUtil.startsWith(FOOBAR, ""), "startsWith(FOOBAR, \"\")"

            assert StringUtil.startsWith(foobar, foo), "startsWith(foobar, foo)"
            assert StringUtil.startsWith(FOOBAR, FOO), "startsWith(FOOBAR, FOO)"
            assert !StringUtil.startsWith(foobar, FOO), "startsWith(foobar, FOO)"
            assert !StringUtil.startsWith(FOOBAR, foo), "startsWith(FOOBAR, foo)"

            assert !StringUtil.startsWith(foo, foobar), "startsWith(foo, foobar)"
            assert !StringUtil.startsWith(bar, foobar), "startsWith(foo, foobar)"

            assert !StringUtil.startsWith(foobar, bar), "startsWith(foobar, bar)"
            assert !StringUtil.startsWith(FOOBAR, BAR), "startsWith(FOOBAR, BAR)"
            assert !StringUtil.startsWith(foobar, BAR), "startsWith(foobar, BAR)"
            assert !StringUtil.startsWith(FOOBAR, bar), "startsWith(FOOBAR, bar)"
    }

    def "StartsWithIgnoreCase"() {
        given:
            String foo    = "foo";
            String bar    = "bar";
            String foobar = "foobar";
            String FOO    = "FOO";
            String BAR    = "BAR";
            String FOOBAR = "FOOBAR";
        expect:
            assert StringUtil.startsWithIgnoreCase(null, null), "startsWith(null, null)"
            assert !StringUtil.startsWithIgnoreCase(FOOBAR, null), "startsWith(FOOBAR, null)"
            assert !StringUtil.startsWithIgnoreCase(null, FOO), "startsWith(null, FOO)"
            assert StringUtil.startsWithIgnoreCase(FOOBAR, ""), "startsWith(FOOBAR, \"\")"

            assert StringUtil.startsWithIgnoreCase(foobar, foo), "startsWith(foobar, foo)"
            assert StringUtil.startsWithIgnoreCase(FOOBAR, FOO), "startsWith(FOOBAR, FOO)"
            assert StringUtil.startsWithIgnoreCase(foobar, FOO), "startsWith(foobar, FOO)"
            assert StringUtil.startsWithIgnoreCase(FOOBAR, foo), "startsWith(FOOBAR, foo)"

            assert !StringUtil.startsWithIgnoreCase(foo, foobar), "startsWith(foo, foobar)"
            assert !StringUtil.startsWithIgnoreCase(bar, foobar), "startsWith(foo, foobar)"

            assert !StringUtil.startsWithIgnoreCase(foobar, bar), "startsWith(foobar, bar)"
            assert !StringUtil.startsWithIgnoreCase(FOOBAR, BAR), "startsWith(FOOBAR, BAR)"
            assert !StringUtil.startsWithIgnoreCase(foobar, BAR), "startsWith(foobar, BAR)"
            assert !StringUtil.startsWithIgnoreCase(FOOBAR, bar), "startsWith(FOOBAR, bar)"
    }

    def "ContainsIgnoreCase"() {
        assert !StringUtil.containsIgnoreCase(null, null)

        // Null tests
        assert !StringUtil.containsIgnoreCase(null, "")
        assert !StringUtil.containsIgnoreCase(null, "a")
        assert !StringUtil.containsIgnoreCase(null, "abc")

        assert !StringUtil.containsIgnoreCase("", null)
        assert !StringUtil.containsIgnoreCase("a", null)
        assert !StringUtil.containsIgnoreCase("abc", null)

        // Match len = 0
        assert StringUtil.containsIgnoreCase("", "")
        assert StringUtil.containsIgnoreCase("a", "")
        assert StringUtil.containsIgnoreCase("abc", "")

        // Match len = 1
        assert !StringUtil.containsIgnoreCase("", "a")
        assert StringUtil.containsIgnoreCase("a", "a")
        assert StringUtil.containsIgnoreCase("abc", "a")
        assert !StringUtil.containsIgnoreCase("", "A")
        assert StringUtil.containsIgnoreCase("a", "A")
        assert StringUtil.containsIgnoreCase("abc", "A")

        // Match len > 1
        assert !StringUtil.containsIgnoreCase("", "abc")
        assert !StringUtil.containsIgnoreCase("a", "abc")
        assert StringUtil.containsIgnoreCase("xabcz", "abc")
        assert !StringUtil.containsIgnoreCase("", "ABC")
        assert !StringUtil.containsIgnoreCase("a", "ABC")
        assert StringUtil.containsIgnoreCase("xabcz", "ABC")
    }

    def "ConvertToEnvironmentVarName"() {
        given:
            String propName = "property.one.name"
        when:
            String envVarName = StringUtil.convertToEnvironmentVarName(propName)
        then:
            assert envVarName == "PROPERTY_ONE_NAME"
        when:
            propName = "\${property.name}"
            envVarName = StringUtil.convertToEnvironmentVarName(propName)
        then:
            assert envVarName == "\${PROPERTY_NAME}"

    }
}
