/*
 * Copyright 2020 Tomcat-Vault contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.tomcat.vault.util

import spock.lang.Specification

import javax.crypto.SecretKey
import java.nio.file.Paths
import java.security.KeyStore

/**
 * Unit test for {@link org.apache.tomcat.vault.util.KeyStoreUtil}
 * @author danluca
 * @trace org.apache.tomcat.vault.util.KeyStoreUtil
 */
class KeyStoreUtilTest extends Specification {

    static def ksPath = "/org/apache/tomcat/vault/security/vault/conf/vault.p12"

    def "GetKeyStore-File"() {
        given:
            def filePath = Paths.get(KeyStoreUtilTest.class.getResource(ksPath).toURI()).toAbsolutePath().toString()
        when:
            KeyStore ks = KeyStoreUtil.getKeyStore("PKCS12", new File(filePath), "Guidant!4".toCharArray())
        then:
            assert ks
            assert ks.aliases().collect().size() == 2
    }

    def "GetKeyStore-Url"() {
        given:
            def urlPath = Paths.get(KeyStoreUtilTest.class.getResource(ksPath).toURI()).toAbsolutePath().toString()
        when:
            KeyStore ks = KeyStoreUtil.getKeyStore("PKCS12", urlPath, "Guidant!4".toCharArray())
        then:
            assert ks
            assert ks.aliases().collect().size() == 2
    }

    def "GetKeyStore-Stream"() {
        given:
            def stream = KeyStoreUtilTest.class.getResourceAsStream(ksPath)
        when:
            KeyStore ks = KeyStoreUtil.getKeyStore("PKCS12", stream, "Guidant!4".toCharArray())
        then:
            assert ks
            assert ks.aliases().collect().size() == 2
    }

    def "GenerateKey"() {
        when:
            SecretKey key128 = KeyStoreUtil.generateKey(128)
            SecretKey key192 = KeyStoreUtil.generateKey(192)
            SecretKey key256 = KeyStoreUtil.generateKey(256)
        then:
            assert key128.encoded.size() == 128/8
            assert key192.encoded.size() == 192/8
            assert key256.encoded.size() == 256/8
        when:
            KeyStoreUtil.generateKey(820)
        then:
            thrown(Exception)
    }
}
