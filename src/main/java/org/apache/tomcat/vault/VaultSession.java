/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault;

import org.apache.tomcat.vault.security.Util;
import org.apache.tomcat.vault.security.plugins.PBEUtils;
import org.apache.tomcat.vault.security.vault.PicketBoxSecurityVault;
import org.apache.tomcat.vault.security.vault.SecurityVault;
import org.apache.tomcat.vault.security.vault.SecurityVaultException;
import org.apache.tomcat.vault.security.vault.SecurityVaultFactory;
import org.apache.tomcat.vault.util.Constants;
import org.apache.tomcat.vault.util.EncryptionUtil;
import org.apache.tomcat.vault.util.PropertySourceVault;
import org.apache.tomcat.vault.util.StringUtil;

import javax.crypto.SecretKey;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

/**
 * Non-interactive session for {@link VaultTool}
 *
 * @author Peter Skopek
 * @author Dan Luca
 */
public final class VaultSession {
    private final String keystoreURL;
    private final String keystorePassword;
    private String keystoreMaskedPassword;
    private final String keystoreType;
    private final String encryptionDirectory;
    private final String salt;
    private final int iterationCount;

    private SecurityVault vault;
    private final PBEUtils pbe;
    private String vaultAlias;
    private String inlineAlias;

    /**
     * Constructor to create VaultSession.
     *
     * @param keystoreURL
     * @param keystorePassword
     * @param encryptionDirectory
     * @param salt
     * @param iterationCount
     * @throws Exception
     */
    public VaultSession(String keystoreURL, String keystorePassword, String keystoreType, String encryptionDirectory, String salt, int iterationCount) {
        this.keystoreURL = keystoreURL;
        this.keystorePassword = keystorePassword;
        this.keystoreType = keystoreType;
        this.encryptionDirectory = encryptionDirectory;
        this.salt = salt;
        this.iterationCount = iterationCount;
        validate();
        this.pbe = new PBEUtils(this.salt, this.iterationCount);
    }

    /**
     * Validate fields sent to this class's constructor.
     */
    private void validate() {
        validateKeystoreURL();
        validateEncryptionDirectory();
        validateSalt();
        validateIterationCount();
        validateKeystorePassword();
    }

    protected void validateKeystoreURL() {

        File f = new File(keystoreURL);
        if (!f.exists()) {
            throw new IllegalArgumentException("Keystore [" + keystoreURL + "] doesn't exist."
                    + "\nkeystore could be created: keytool -genseckey -alias vault -storetype pkcs12 -keyalg AES -keysize 256 -keystore "
                    + keystoreURL
                    + "\nkeystore could be created: keytool -genseckey -alias inline -storetype pkcs12 -keyalg AES -keysize 256 -keystore "
                    + keystoreURL);
        } else if (!f.isFile()) {
            throw new IllegalArgumentException("Keystore [" + keystoreURL + "] is not a file.");
        }
    }

    protected void validateKeystorePassword() {
        if (keystorePassword == null) {
            throw new IllegalArgumentException("Keystore password has to be specified.");
        }
    }

    protected void validateEncryptionDirectory() {
        if (encryptionDirectory == null) {
            throw new IllegalArgumentException("Encryption directory has to be specified.");
        }
        File d = new File(encryptionDirectory);
        if (!d.isDirectory()) {
            if (!d.mkdirs()) {
                throw new IllegalArgumentException("Cannot create encryption directory " + d.getAbsolutePath());
            }
        }
        if (!d.isDirectory()) {
            throw new IllegalArgumentException("Encryption directory is not a directory or doesn't exist. (" + encryptionDirectory + ")");
        }
    }

    protected void validateIterationCount() {
        if (iterationCount < 1) {
            throw new IllegalArgumentException("Iteration count has to be within 1 - " + Integer.MAX_VALUE + ", but is " + iterationCount);
        }
    }

    protected void validateSalt() {
        if (StringUtil.isEmpty(salt)) {
            throw new IllegalArgumentException("Salt has to be defined.");
        }
    }

    /**
     * Initialize the underlying vault.
     *
     * @throws RuntimeException
     */
    private void initSecurityVault() {
        try {
            this.vault = SecurityVaultFactory.get();
            this.vault.init(getVaultOptionsMap());
        } catch (SecurityVaultException e) {
            throw new RuntimeException("Exception encountered: " + e.getMessage(), e);
        }
    }

    /**
     * Start the vault with given alias.
     *
     * @param vaultAlias
     * @param inlineAlias
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public void startVaultSession(String vaultAlias, String inlineAlias) throws GeneralSecurityException, IOException {
        if (StringUtil.isEmpty(vaultAlias) || StringUtil.isEmpty(inlineAlias)) {
            throw new IllegalArgumentException("Vault and Inline aliases must be specified.");
        }
        this.keystoreMaskedPassword = this.pbe.encode(this.keystorePassword);
        this.vaultAlias = vaultAlias;
        this.inlineAlias = inlineAlias;
        initSecurityVault();
    }

    private Map<String, String> getVaultOptionsMap() {
        Map<String, String> options = new HashMap<>();
        options.put(Constants.KEYSTORE_URL, keystoreURL);
        options.put(Constants.KEYSTORE_PASSWORD, keystoreMaskedPassword);
        options.put(Constants.VAULT_ALIAS, vaultAlias);
        options.put(Constants.INLINE_ALIAS, inlineAlias);
        options.put(Constants.KEYSTORE_TYPE, keystoreType);
        options.put(Constants.KEY_SIZE, Integer.toString(Util.DEFAULT_KEY_SIZE));
        options.put(Constants.SALT, salt);
        options.put(Constants.ITERATION_COUNT, Integer.toString(iterationCount));
        options.put(Constants.ENC_FILE_DIR, encryptionDirectory);
        return options;
    }

    /**
     * Add secured attribute to specified vault block. This method can be called only after successful
     * startVaultSession() call.
     *
     * @param vaultBlock
     * @param attributeName
     * @param attributeValue
     */
    public void addSecuredAttribute(String vaultBlock, String attributeName, String attributeValue) throws SecurityVaultException {
        vault.store(vaultBlock, attributeName, attributeValue);
        attributeCreatedDisplay(vaultBlock, attributeName);
    }

    /**
     * Check whether secured attribute is already set for given vault block and attribute name. This method can be called only after
     * successful startVaultSession() call.
     *
     * @param vaultBlock
     * @param attributeName
     * @return true is password already exists for given vault block and attribute name.
     * @throws Exception
     */
    public boolean checkSecuredAttribute(String vaultBlock, String attributeName) throws SecurityVaultException {
        return vault.exists(vaultBlock, attributeName);
    }

    /**
     * Remove secured attribute with given vault block and attribute name. This method can be called only after
     * successful startVaultSession() call.
     *
     * @param vaultBlock
     * @param attributeName
     * @throws Exception
     */
    public void removeSecuredAttribute(String vaultBlock, String attributeName) throws SecurityVaultException {
        vault.remove(vaultBlock, attributeName);
    }

    public void listVaultKeys() throws SecurityVaultException {
        System.out.println("********************************************");
        System.out.printf("Entries stored in vault %s:%n", vault.getLocation());
        vault.keyList().forEach(k-> {
            System.out.printf("%s%s%s%n", SecurityVault.VAULT_PROPERTY_PLACEHOLDER_PREFIX, k, SecurityVault.DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX);
        });
        System.out.println("********************************************");
    }

    /**
     * Encrypt a value using the inline feature (formerly known as 'CRYPT')
     *
     * @param valueToEncrypt password to encrypt
     *
     * @throws Exception if there is an issue retrieving the keyAlias from the vault.
     */
    public void encryptInlineValue(String valueToEncrypt) throws Exception {
        SecretKey key = vault.getKey(inlineAlias);
        if (key == null) {
            throw new IllegalArgumentException("Keystore does NOT contain a secret key with alias " + inlineAlias);
        }
        System.out.printf("Encrypted value: %s%s%s%n", SecurityVault.VAULT_PROPERTY_PLACEHOLDER_PREFIX, EncryptionUtil.encryptB64(valueToEncrypt, key), SecurityVault.DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX);
        System.out.printf("Ensure to set property %s=%s in vault properties file%n", Constants.INLINE_ALIAS, inlineAlias);
    }

    /**
     * Encrypt a value using the mask feature.
     *
     * @param valueToEncrypt password to encrypt
     *
     * @throws Exception if there is an issue
     */
    public void encryptMaskValue(String valueToEncrypt) throws Exception {
        System.out.printf("Encrypted masked value: %s%n", pbe.encode(valueToEncrypt));
    }

    /**
     * Display info about stored secured attribute.
     *
     * @param vaultBlock
     * @param attributeName
     */
    private void attributeCreatedDisplay(String vaultBlock, String attributeName) {
        System.out.println("Secured attribute value has been stored in vault. ");
        System.out.println("Please make note of the following:");
        System.out.println("********************************************");
        System.out.println("Vault Block:" + vaultBlock);
        System.out.println("Attribute Name:" + attributeName);
        System.out.println("Configuration should be done as follows:");
        System.out.println(SecurityVault.VAULT_PROPERTY_PLACEHOLDER_PREFIX + vaultBlock + SecurityVault.VAULT_SECTION_SEPARATOR + attributeName + SecurityVault.DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX);
        System.out.println("********************************************");
    }

    /**
     * Display info about vault itself in form of AS7 configuration file.
     */
    public void vaultConfigurationDisplay() {
        System.out.println("Vault Configuration in tomcat vault properties file:");
        System.out.println("********************************************");
        outputConfig(System.out);
        System.out.println("********************************************");
    }

    /**
     * Print configuration file to stream.
     *
     * @param out stream to print config.
     */
    public void outputConfig(PrintStream out) {
        //on windows, the file separator char '\' is also the escape character - escape it in the paths, so properties loader can get the proper character
        String path = Paths.get(keystoreURL).toAbsolutePath().normalize().toString();
        path = VaultTool.isWindows() ? path.replace(File.separatorChar, '/') : path;
        out.println("KEYSTORE_URL=" + path);
        out.println("KEYSTORE_PASSWORD=" + keystoreMaskedPassword);
        out.println("KEYSTORE_TYPE=" + keystoreType);
        out.println("VAULT_ALIAS=" + vaultAlias);
        out.println("INLINE_ALIAS=" + inlineAlias);
        out.println("SALT=" + salt);
        out.println("ITERATION_COUNT=" + iterationCount);
        path = Paths.get(encryptionDirectory).toAbsolutePath().normalize().toString();
        path = VaultTool.isWindows() ? path.replace(File.separatorChar, '/') : path;
        out.println("ENC_FILE_DIR=" + path);
    }

}
