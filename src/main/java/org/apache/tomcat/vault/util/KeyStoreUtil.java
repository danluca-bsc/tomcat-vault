/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2011, Red Hat Middleware LLC, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault.util;

import org.apache.tomcat.util.res.StringManager;
import org.apache.tomcat.vault.security.Util;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;

/**
 * Utility to handle Java Keystore
 *
 * @author Anil.Saldhana@redhat.com
 * @author Peter Skopek (pskopek_at_redhat_dot_com)
 */
public abstract class KeyStoreUtil {
    private static final StringManager msm = StringManager.getManager("org.apache.tomcat.vault.security.resources");

    /**
     * Get the KeyStore
     *
     * @param keyStoreType or null for default
     * @param keyStoreFile
     * @param storePass
     * @return
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public static KeyStore getKeyStore(String keyStoreType, File keyStoreFile, char[] storePass) throws GeneralSecurityException, IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(keyStoreFile);
            return getKeyStore(keyStoreType, fis, storePass);
        } finally {
            Util.safeClose(fis);
        }
    }

    /**
     * Get the Keystore given the url to the keystore file as a string
     *
     * @param keyStoreType or null for default
     * @param fileURL
     * @param storePass
     * @return
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public static KeyStore getKeyStore(String keyStoreType, String fileURL, char[] storePass) throws GeneralSecurityException, IOException {
        if (fileURL == null)
            throw new IllegalArgumentException(msm.getString("invalidNullArgument", "fileURL"));

        File file = new File(fileURL);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            return getKeyStore(keyStoreType, fis, storePass);
        } finally {
            Util.safeClose(fis);
        }
    }

    /**
     * Get the Key Store
     * <b>Note:</b> This method wants the InputStream to be not null.
     *
     * @param keyStoreType or null for default
     * @param ksStream
     * @param storePass
     * @return
     * @throws GeneralSecurityException
     * @throws IOException
     * @throws IllegalArgumentException if ksStream is null
     */
    public static KeyStore getKeyStore(String keyStoreType, InputStream ksStream, char[] storePass) throws GeneralSecurityException, IOException {
        if (ksStream == null)
            throw new IllegalArgumentException(msm.getString("invalidNullArgument", "ksStream"));
        KeyStore ks = KeyStore.getInstance((keyStoreType == null ? KeyStore.getDefaultType() : keyStoreType));
        ks.load(ksStream, storePass);
        return ks;
    }

    public static SecretKey generateKey(int keySize) throws NoSuchAlgorithmException {
        KeyGenerator kgen = KeyGenerator.getInstance(Util.SECRET_KEY_TYPE);
        kgen.init(keySize);
        SecretKey key = kgen.generateKey();
        return key;
    }
}
