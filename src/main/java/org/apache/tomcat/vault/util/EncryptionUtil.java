/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2008, Red Hat Middleware LLC, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault.util;

import org.apache.tomcat.vault.security.Util;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;

/**
 * Encryption/Decryption utility using {@value #ALG} algorithm
 *
 * @author Anil.Saldhana@redhat.com
 * @author Dan Luca @ BSC
 */
public abstract class EncryptionUtil {
    private static final SecureRandom random = new SecureRandom();
    private static final String ALG = "AES/GCM/PKCS5Padding";
    private static final int IV_SIZE = 12;
    private static final int TAG_SIZE = 16;

    public static byte[] encrypt(byte[] data, SecretKey key) throws GeneralSecurityException, IOException {
        byte[] iv = new byte[IV_SIZE];
        random.nextBytes(iv);
        AlgorithmParameterSpec paramSpec = new GCMParameterSpec(TAG_SIZE*8, iv);
        Cipher cipher = Cipher.getInstance(ALG);
        cipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
        byte[] encData = cipher.doFinal(data);

        //mix/scramble the IV into the output
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(encData, 0, 2);
        baos.write(iv);
        baos.write(encData ,2, encData.length-2);

        return baos.toByteArray();
    }

    public static String encryptB64(String data, SecretKey key) throws GeneralSecurityException, IOException {
        byte[] encData = encrypt(data.getBytes(Util.UTF8), key);
        return Base64.getEncoder().encodeToString(encData);
    }


    public static byte[] decrypt(byte[] encryptedData, SecretKey key) throws GeneralSecurityException, IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(encryptedData);
        byte[] iv = new byte[IV_SIZE];
        byte[] encData = new byte[encryptedData.length-IV_SIZE];
        //extract IV and encrypted data - this descramble must match the encrypt function, on purpose not documented
        bais.read(encData, 0, 2);
        bais.read(iv, 0, IV_SIZE);
        bais.read(encData, 2 ,encryptedData.length-IV_SIZE-2);

        AlgorithmParameterSpec paramSpec = new GCMParameterSpec(TAG_SIZE*8, iv);
        Cipher cipher = Cipher.getInstance(ALG);
        cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
        return cipher.doFinal(encData);
    }

    public static String decryptB64(String b64Data, SecretKey key) throws GeneralSecurityException, IOException {
        byte[] data = decrypt(Base64.getDecoder().decode(b64Data), key);
        return new String(data, Util.UTF8);
    }

}