/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2008, Red Hat Middleware LLC, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors. 
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault.util;

import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;

/**
 * Privileged Blocks
 *
 * @author Anil.Saldhana@redhat.com
 * @since Dec 9, 2008
 */
class SecurityActions {
    static Class<?> loadClass(final Class<?> theClass, final String fqn) {
        return AccessController.doPrivileged((PrivilegedAction<Class<?>>) () -> {
            ClassLoader classLoader = theClass.getClassLoader();

            Class<?> clazz = loadClass(classLoader, fqn);
            if (clazz == null) {
                classLoader = Thread.currentThread().getContextClassLoader();
                clazz = loadClass(classLoader, fqn);
            }
            return clazz;
        });
    }

    static Class<?> loadClass(final ClassLoader cl, final String fqn) {
        return AccessController.doPrivileged((PrivilegedAction<Class<?>>) () -> {
            try {
                return cl.loadClass(fqn);
            } catch (ClassNotFoundException e) {
            }
            return null;
        });
    }

    /**
     * Set the system property
     *
     * @param key
     * @param value
     * @return
     */
    static void setSystemProperty(final String key, final String value) {
        AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
            System.setProperty(key, value);
            return null;
        });
    }

    /**
     * Get the environment if exists, or system property (in this order)
     *
     * @param key property to retrieve
     * @param defaultValue default value if neither environment variable nor system property exists
     * @return
     */
    static String getSystemProperty(final String key, final String defaultValue) {
        String envKey = StringUtil.convertToEnvironmentVarName(key);
        String value = AccessController.doPrivileged((PrivilegedAction<String>) () -> System.getenv(envKey));
        if (StringUtil.isEmpty(value)) {
            value = AccessController.doPrivileged((PrivilegedAction<String>) () -> System.getProperty(key, defaultValue));
        }
        return value;
    }

    /**
     * Load a resource based on the passed {@link Class} classloader.
     * Failing which try with the Thread Context CL
     *
     * @param clazz
     * @param resourceName
     * @return
     */
    static URL loadResource(final Class<?> clazz, final String resourceName) {
        return AccessController.doPrivileged((PrivilegedAction<URL>) () -> {
            ClassLoader clazzLoader = clazz.getClassLoader();
            URL url = clazzLoader.getResource(resourceName);

            if (url == null) {
                clazzLoader = Thread.currentThread().getContextClassLoader();
                url = clazzLoader.getResource(resourceName);
            }

            return url;
        });
    }
}