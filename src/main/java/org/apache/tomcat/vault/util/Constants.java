/*
 * Copyright 2020 Tomcat-Vault contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.tomcat.vault.util;

/**
 * Collection of constants
 *
 * @author Dan Luca
 */
public abstract class Constants {
    // options
    public static final String ENC_FILE_DIR = "ENC_FILE_DIR";
    public static final String KEYSTORE_URL = "KEYSTORE_URL";
    public static final String KEYSTORE_PASSWORD = "KEYSTORE_PASSWORD";
    public static final String VAULT_ALIAS = "VAULT_ALIAS";
    public static final String INLINE_ALIAS = "INLINE_ALIAS";
    public static final String SALT = "SALT";
    public static final String ITERATION_COUNT = "ITERATION_COUNT";
    public static final String KEY_SIZE = "KEY_SIZE";
    public static final String KEYSTORE_TYPE = "KEYSTORE_TYPE";
    public static final String DEFAULT_PROPERTY_PLACEHOLDER_PREFIX = "${";
    public static final String DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX = "}";
    public static final String PROPERTY_DEFAULT_SEPARATOR = ":";
    public static final String OVERRIDE_PROPERTY_PREFIX = "org.apache.tomcat.vault.util.";
}
