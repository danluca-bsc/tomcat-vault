/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault.util;

import org.apache.catalina.Globals;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.IntrospectionUtils.PropertySource;
import org.apache.tomcat.vault.security.vault.SecurityVault;
import org.apache.tomcat.vault.security.vault.SecurityVaultFactory;

import javax.crypto.SecretKey;
import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Property Source that handles property resolution from the Vault during Tomcat bootstrap. Supports delegation, nested placeholders,
 * overrides from system properties and environment variables.
 *
 * @author Dan Luca
 */
public class PropertySourceVault implements PropertySource {
    private static final Log log = LogFactory.getLog(PropertySourceVault.class);

    private static final String PROPERTY_FILE_RELATIVE_PATH = "/conf/vault.properties";
    private static final String CATALINA_FILE_RELATIVE_PATH = "/conf/catalina.properties";

    private final PropertyFileManager vaultPfm;
    private final PropertyFileManager catalinaPfm;
    private SecurityVault vault;
    private Properties vaultProps;
    private Properties catalinaProps;
    private SecretKey inlineKey;

    public PropertySourceVault() {
        this.vault = null;
        this.vaultProps = null;

        String catalinaHome = System.getProperty(Globals.CATALINA_HOME_PROP);
        String catalinaBase = System.getProperty(Globals.CATALINA_BASE_PROP);
        String catalina = null;

        if (new File(catalinaBase + PROPERTY_FILE_RELATIVE_PATH).canRead()) {
            catalina = catalinaBase;
            log.debug("vault.properties found in catalina.base [" + catalina + "]");
        } else if (new File(catalinaHome + PROPERTY_FILE_RELATIVE_PATH).canRead()) {
            // Using catalina.home is kept for backwards compat
            catalina = catalinaHome;
            log.debug("vault.properties found in catalina.home [" + catalina + "]");
        } else {
            // Always default to catalina.base if it doesn't exist in either place
            catalina = catalinaBase;
            log.debug("vault.properties not found, using catalina.base [" + catalina + "]");
        }

        String vaultPropertiesPath = Paths.get(catalina, PROPERTY_FILE_RELATIVE_PATH).toString();
        String vaultProperties = System.getProperty(Constants.OVERRIDE_PROPERTY_PREFIX + "VAULT_PROPERTIES");
        if (!StringUtil.isEmpty(vaultProperties)) {
            vaultPropertiesPath = vaultProperties;
        }

        this.vaultPfm = new PropertyFileManager(vaultPropertiesPath);
        //catalina properties
        String catalinaPropsPath = Paths.get(catalina, CATALINA_FILE_RELATIVE_PATH).toString();
        this.catalinaPfm = new PropertyFileManager(catalinaPropsPath);

        this.init();
    }

    protected void init() {
        try {
            vault = SecurityVaultFactory.get();

            // Load vault and catalina property files
            vaultProps = vaultPfm.load();
            catalinaProps = catalinaPfm.load();

            // If properties is null then there was an exception
            if (vaultProps == null) {
                return;
            }

            Map<String, String> options = new HashMap<>();
            options.put(Constants.KEYSTORE_URL, vaultProps.getProperty(Constants.KEYSTORE_URL));
            options.put(Constants.KEYSTORE_PASSWORD, vaultProps.getProperty(Constants.KEYSTORE_PASSWORD));
            options.put(Constants.VAULT_ALIAS, vaultProps.getProperty(Constants.VAULT_ALIAS));
            options.put(Constants.INLINE_ALIAS, vaultProps.getProperty(Constants.INLINE_ALIAS));
            options.put(Constants.KEYSTORE_TYPE, vaultProps.getProperty(Constants.KEYSTORE_TYPE));
            options.put(Constants.SALT, vaultProps.getProperty(Constants.SALT));
            options.put(Constants.ITERATION_COUNT, vaultProps.getProperty(Constants.ITERATION_COUNT));
            options.put(Constants.ENC_FILE_DIR, vaultProps.getProperty(Constants.ENC_FILE_DIR));

            vault.init(options);

            this.inlineKey = vault.getKey(vaultProps.getProperty(Constants.INLINE_ALIAS));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Resolve a property value against this vault - default values are not supported
     * @param propValue
     * @return
     */
    @Override
    public String getProperty(String propValue) {
        // If the vault failed to init, then return without change
        if (!vault.isInitialized()) {
            return propValue;
        }
        if (StringUtil.isEmpty(propValue)) {
            return propValue;
        }

        //if this resolver returns null, then the next property source resolver makes an attempt
        String result = null;
        if (StringUtil.containsIgnoreCase(propValue, SecurityVault.VAULT_PROPERTY_PLACEHOLDER_PREFIX)) {
            //we have either a vault property or an inline one. the vault property contains the "::" separator between block and attribute name
            Pattern pattern = Pattern.compile(String.format("%s([^%s]+)%s", SecurityVault.RE_VAULT_PROPERTY_PLACEHOLDER_PREFIX,
                SecurityVault.RE_DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX, SecurityVault.RE_DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX));
            Matcher matcher = pattern.matcher(propValue);

            StringBuffer buffer = new StringBuffer();
            while (matcher.find()) {
                String subString = matcher.group(1);
                matcher.appendReplacement(buffer, resolveVaultProperty(subString));
            }
            matcher.appendTail(buffer);
            result = buffer.toString();
        } else {
            result = resolvePropertyFromEnvironment(propValue);
        }

        return result;
    }

    /**
     * Internal resolver for property values that ARE vault stored/inlined. The value of the handle is in the un-decorated form:
     * <ul>
     *     <li>block and attribute name: {@code block_name:attribute_name} </li>
     *     <li>inlined encrypted value: {@code b64_encrypted_value}</li>
     * </ul>
     * If the handle cannot be resolved against the vault, the decorated form of the handle is returned as to preserve the value of the (unresolved) placeholder
     * @param handle the vault handle to resolve, see above for the formats
     * @return the decrypted value of the handle if successful; the vault prefix/suffix decorated handle if not
     */
    protected String resolveVaultProperty(String handle) {
        // if we can't resolve the handle, return it decorated such that the placeholder preserves the original unresolved string - e.g. '#vault[block:attribute]'
        String value = SecurityVault.VAULT_PROPERTY_PLACEHOLDER_PREFIX + handle + SecurityVault.DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX;
        try {
            //Look for vault attribute names vs inline values
            if (handle.contains(SecurityVault.VAULT_SECTION_SEPARATOR)) {
                //we have a vault attribute name
                String[] vaultNames = handle.split(SecurityVault.VAULT_SECTION_SEPARATOR);
                if (vaultNames.length == 2) {
                    value = vault.retrieve(vaultNames[0], vaultNames[1]);
                } else {
                    log.warn(String.format("Vault attribute name '%s' does not exist - value used unprocessed", value));
                }
            } else {
                //we have an inline value
                value = EncryptionUtil.decryptB64(handle, this.inlineKey);
            }
        } catch (Exception e) {
            log.error(String.format("Cannot decrypt %s, placeholder left unchanged %s - %s: %s", handle, value, e.getClass().getSimpleName(), e.getMessage()), e);
        }
        return value;
    }

    /**
     * Internal resolver for property values that do NOT match a vault related value (stored or inlined). This is NOT intended as a general means to resolve
     * a property from system/environment without prior checking for being vault stored/inlined.
     * @param property property name to resolve
     * @return the value of the resolved property
     */
    protected String resolvePropertyFromEnvironment(String property) {
        String propertyName = property;
        String defaultValue = null;
        int defIndex = property.indexOf(Constants.PROPERTY_DEFAULT_SEPARATOR);
        if (defIndex > 0) {
            propertyName = property.substring(0, defIndex);
            defaultValue = property.substring(defIndex + Constants.PROPERTY_DEFAULT_SEPARATOR.length());
        }
        //if the property is defined in environment or system properties, then use that value as it has higher priority
        //providing null for default value on purpose to have an indication whether the property is defined at these levels
        String value = SecurityActions.getSystemProperty(property, null);
        if (StringUtil.isEmpty(value)) {
            //property not defined in the environment/system properties - look into catalina properties as a last resort; use default value determined above otherwise
            value = catalinaProps.getProperty(propertyName, defaultValue);
        }

        //if the resolved value is compounded, keep resolving
        if (StringUtil.containsIgnoreCase(value, SecurityVault.VAULT_PROPERTY_PLACEHOLDER_PREFIX)) {
            value = getProperty(value);
        }
        if (StringUtil.containsIgnoreCase(value, Constants.DEFAULT_PROPERTY_PLACEHOLDER_PREFIX)) {
            //taking a shortcut for defining the regex pattern for '${' as these characters need escaped (as opposed to building the pattern from Constants prefix/suffixes)
            Pattern reProp = Pattern.compile("\\$\\{([^}]+)}", Pattern.CASE_INSENSITIVE);
            Matcher m = reProp.matcher(value);
            StringBuffer sb = new StringBuffer();
            while (m.find()) {
                String subString = m.group(1);
                String propValue = resolvePropertyFromEnvironment(subString);
                m.appendReplacement(sb, propValue);
            }
            m.appendTail(sb);
            value = sb.toString();
        }
        //if after all these efforts the value resolves to null - then return the property name to indicate it cannot be resolved (aids in debugging)
        return Optional.ofNullable(value).orElse(property);
    }

}
