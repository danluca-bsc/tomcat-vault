/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2011, Red Hat, Inc., and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault;

import org.apache.tomcat.vault.security.vault.SecurityVault;
import org.apache.tomcat.vault.util.StringUtil;

import java.io.Console;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Interaction with initialized {@link SecurityVault} via the {@link VaultTool}
 *
 * @author Anil Saldhana
 * @author Dan Luca
 */
public class VaultInteraction {

    private VaultSession vaultNISession;

    public VaultInteraction(VaultSession vaultSession) {
        this.vaultNISession = vaultSession;
    }

    public void start() {
        Console console = System.console();

        if (console == null) {
            System.err.println("No console.");
            System.exit(1);
        }

        Scanner in = new Scanner(System.in);
        while (true) {
            String commandStr = String.format("Please select an option::%n  0: Encrypt an attribute%n  1: Check whether an attribute name is stored in the vault%n"
                    + "  2: Remove an attribute from vault%n  3: List attribute names stored in the vault%n  4: Encrypt a sensitive value for inline usage%n"
                    + "  5: Encrypt a sensitive value for masked usage%n  6: Generate configuration file%n  Other: Exit");

            System.out.println(commandStr);
            int choice = in.nextInt();
            switch (choice) {
                case 0:
                    System.out.println("Task: Store a secured attribute");
                    String attributeValue = VaultInteractiveSession.getSensitiveValue("Please enter secured attribute value (such as password)");
                    String vaultBlock = null;

                    while (StringUtil.isEmpty(vaultBlock)) {
                        vaultBlock = console.readLine("Enter Vault Block:");
                    }

                    String attributeName = null;

                    while (StringUtil.isEmpty(attributeName)) {
                        attributeName = console.readLine("Enter Attribute Name:");
                    }
                    try {
                        vaultNISession.addSecuredAttribute(vaultBlock, attributeName, attributeValue);
                    } catch (Exception e) {
                        System.out.println("Exception occurred:" + e.getLocalizedMessage());
                    }
                    break;
                case 1:
                    System.out.println("Task: Verify whether a secured attribute exists");
                    try {
                        vaultBlock = null;

                        while (StringUtil.isEmpty(vaultBlock)) {
                            vaultBlock = console.readLine("Enter Vault Block:");
                        }

                        attributeName = null;

                        while (StringUtil.isEmpty(attributeName)) {
                            attributeName = console.readLine("Enter Attribute Name:");
                        }
                        if (vaultNISession.checkSecuredAttribute(vaultBlock, attributeName))
                            System.out.println("A value exists for (" + vaultBlock + ", " + attributeName + ")");
                        else
                            System.out.println("No value has been store for (" + vaultBlock + ", " + attributeName + ")");
                    } catch (Exception e) {
                        System.out.println("Exception occurred:" + e.getLocalizedMessage());
                    }
                    break;
                case 2:
                    System.out.println("Task: Remove a secured attribute");
                    try {
                        vaultBlock = null;

                        while (StringUtil.isEmpty(vaultBlock)) {
                            vaultBlock = console.readLine("Enter Vault Block:");
                        }

                        attributeName = null;

                        while (StringUtil.isEmpty(attributeName)) {
                            attributeName = console.readLine("Enter Attribute Name:");
                        }
                        vaultNISession.removeSecuredAttribute(vaultBlock, attributeName);
                    } catch (Exception e) {
                        System.out.println("Exception occurred:" + e.getLocalizedMessage());
                    }
                    break;
                case 3:
                    System.out.println("Task: List attribute names stored in the vault");
                    try {
                        vaultNISession.listVaultKeys();
                    } catch (Exception e) {
                        System.out.println("Exception occurred: " + e.getLocalizedMessage());
                    }
                    break;
                case 4:
                    System.out.println("Task: Encrypt a sensitive value for inline usage");
                    try {
                        String psw = new String(console.readPassword("Enter password to encrypt (inline): "));
                        vaultNISession.encryptInlineValue(psw);
                    } catch (Exception e) {
                        System.out.println("Exception occurred: " + e.getLocalizedMessage());
                    }
                    break;
                case 5:
                    System.out.println("Task: Encrypt a sensitive value for masked usage");
                    try {
                        String psw = new String(console.readPassword("Enter password to encrypt (mask): "));
                        vaultNISession.encryptMaskValue(psw);
                    } catch (Exception e) {
                        System.out.println("Exception occurred: " + e.getLocalizedMessage());
                    }
                    break;
                case 6:
                    System.out.println("Task: Generate configuration file");
                    try {
                        String cfgFilePath = console.readLine("Enter Config File absolute path:");
                        try(PrintStream ps = new PrintStream(cfgFilePath)) {
                            vaultNISession.outputConfig(ps);
                        }
                    } catch (Exception e) {
                        System.out.println("Exception occurred: " + e.getLocalizedMessage());
                    }
                    break;
                default:
                    System.exit(0);
            }
        }
    }
}
