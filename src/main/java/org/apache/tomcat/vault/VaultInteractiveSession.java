/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2011, Red Hat, Inc., and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault;

import java.io.Console;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * An interactive session for {@link VaultTool}
 *
 * @author Anil Saldhana
 * @author Dan Luca
 */
public class VaultInteractiveSession {

    private String salt, keystoreURL, keystoreType, encDir, vaultAlias, inlineAlias;
    private int iterationCount = 0;

    // vault non-interactive session
    private VaultSession vaultNISession = null;

    public VaultInteractiveSession() {
    }

    public void start() {
        Console console = System.console();

        if (console == null) {
            System.err.println("No console.");
            System.exit(1);
        }

        while (encDir == null || encDir.length() == 0) {
            encDir = console.readLine("Enter directory to store encrypted files:");
        }
        encDir = Paths.get(encDir).toAbsolutePath().normalize().toString();
        console.printf("\tDirectory resolved to %s%n", encDir);

        while (keystoreURL == null || keystoreURL.length() == 0) {
            keystoreURL = console.readLine("Enter Keystore Location:");
        }
        keystoreURL = Paths.get(keystoreURL).toAbsolutePath().normalize().toString();
        console.printf("\tKeystore location resolved to %s%n", keystoreURL);

        while (keystoreType == null || !(keystoreType.equals("JCEKS") || keystoreType.equals("PKCS12"))) {
            keystoreType = console.readLine("Enter Keystore Type (JCEKS or PKCS12):");
        }

        String keystorePasswd = getSensitiveValue("Enter Keystore password");

        try {
            while (salt == null || salt.length() < 8) {
                salt = console.readLine("Enter at least 8 character salt:");
            }

            String ic = console.readLine("Enter iteration count as a number (Eg: 44):");
            iterationCount = Integer.parseInt(ic);
            vaultNISession = new VaultSession(keystoreURL, keystorePasswd, keystoreType, encDir, salt, iterationCount);

            while (vaultAlias == null || vaultAlias.length() == 0) {
                vaultAlias = console.readLine("Enter Vault key Alias:");
            }

            while (inlineAlias == null || inlineAlias.length() == 0) {
                inlineAlias = console.readLine("Enter Inline key Alias:");
            }

            console.printf("Initializing Vault %s/%s...%n", encDir, "VAULT.dat");
            vaultNISession.startVaultSession(vaultAlias, inlineAlias);
            vaultNISession.vaultConfigurationDisplay();

            console.printf("Vault is initialized and ready for use%n");

            VaultInteraction vaultInteraction = new VaultInteraction(vaultNISession);
            vaultInteraction.start();
        } catch (Exception e) {
            System.err.printf("Exception encountered - %s: %s%n", e.getClass().getSimpleName(), e.getMessage());
        }
    }

    public static String getSensitiveValue(String passwordPrompt) {
        while (true) {
            if (passwordPrompt == null)
                passwordPrompt = "Enter your password";

            Console console = System.console();

            char[] passwd = console.readPassword(passwordPrompt + ": ");
            char[] passwd1 = console.readPassword(passwordPrompt + " again: ");
            boolean noMatch = !Arrays.equals(passwd, passwd1);
            if (noMatch)
                console.printf("Values entered don't match%n");
            else {
                console.printf("\tValues match%n");
                return new String(passwd);
            }
        }
    }

}
