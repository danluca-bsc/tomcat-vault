/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2008, Red Hat Middleware LLC, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors. 
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault.security.vault;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.res.StringManager;
import org.apache.tomcat.vault.security.Util;
import org.apache.tomcat.vault.security.plugins.PBEUtils;
import org.apache.tomcat.vault.util.Constants;
import org.apache.tomcat.vault.util.EncryptionUtil;
import org.apache.tomcat.vault.util.KeyStoreUtil;
import org.apache.tomcat.vault.util.StringUtil;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStore.Entry;
import java.security.KeyStoreException;
import java.util.Map;
import java.util.Set;

/**
 * An instance of {@link SecurityVault} that uses
 * a {@link KeyStore}
 * The shared key just uses a concatenation of a {@link java.util.UUID}
 * and a keystore alias.
 * <p>
 * The following options are expected in the {@link SecurityVault#init(Map)} call:
 * ENC_FILE_DIR: the location where the encoded files will be kept. End with "/" or "\" based on your platform
 * KEYSTORE_URL: location where your keystore is located
 * KEYSTORE_PASSWORD: keystore password.
 * 'plain text' masked password (has to be surrounded within a #mask{...})
 * '{EXT}...' where the '...' is the exact command
 * '{EXTC[:expiration_in_millis]}...' where the '...' is the exact command
 * line that will be passed to the Runtime.exec(String) method to execute a
 * platform command. The first line of the command output is used as the
 * password.
 * EXTC variant will cache the passwords for expiration_in_millis milliseconds.
 * Default cache expiration is 0 = infinity.
 * '{CMD}...' or '{CMDC}...' for a general command to execute. The general
 * command is a string delimited by ',' where the first part is the actual
 * command and further parts represents its parameters. The comma can be
 * backslashed in order to keep it as the part of a parameter.
 * KEYSTORE_ALIAS: Alias where the keypair is located
 * SALT: salt of the masked password. Ensured it is 8 characters in length
 * ITERATION_COUNT: Iteration Count of the masked password.
 * KEY_SIZE: Key size of encryption. Default is 128 bytes.
 * CREATE_KEYSTORE: Whether PicketBox Security Vault has to create missing key store in time of initialization. Default is "FALSE". Implies KEYSTORE_TYPE "JCEKS".
 * KEYSTORE_TYPE: Key store type. Default is JCEKS.
 *
 * @author Anil.Saldhana@redhat.com
 * @author Peter Skopek (pskopek_at_redhat_dot_com)
 * @author Dan Luca
 */
public class PicketBoxSecurityVault implements SecurityVault {
    private static final StringManager sm = StringManager.getManager(PicketBoxSecurityVault.class.getPackage().getName());
    private static final Log log = LogFactory.getLog(PicketBoxSecurityVault.class);
    private static final StringManager msm = StringManager.getManager("org.apache.tomcat.vault.security.resources");

    protected boolean finishedInit = false;
    protected KeyStore keystore = null;
    protected int keySize = 128;
    private char[] keyStorePWD = null;
    private String vaultAlias = null;
    private SecurityVaultData vaultContent = null;
    private SecretKey vaultKey;
    private String vaultDir;
    private String keyStoreType = defaultKeyStoreType;
    private PBEUtils pbe;

    protected static final String VAULT_CONTENT_FILE = "VAULT.dat"; // versioned vault data file
    protected static final String defaultKeyStoreType = "PKCS12";


    /* (non-Javadoc)
     * @see org.jboss.security.vault.SecurityVault#init(java.util.Map)
     */
    public void init(Map<String, String> options) throws SecurityVaultException {
        if (options == null || options.isEmpty())
            throw new IllegalArgumentException(msm.getString("invalidNullOrEmptyOptionMap", "options"));

        String keystoreURL = options.get(Constants.KEYSTORE_URL);
        if (StringUtil.isEmpty(keystoreURL))
            throw new SecurityVaultException(msm.getString("invalidNullOrEmptyOptionMessage", Constants.KEYSTORE_URL));

        keystoreURL = StringUtil.getSystemPropertyAsString(keystoreURL);

        String password = options.get(Constants.KEYSTORE_PASSWORD);
        if (StringUtil.isEmpty(password))
            throw new SecurityVaultException(msm.getString("invalidNullOrEmptyOptionMessage", Constants.KEYSTORE_PASSWORD));
        if (!(StringUtil.startsWithIgnoreCase(password, SecurityVault.MASK_PROPERTY_PLACEHOLDER_PREFIX) || Util.isPasswordCommand(password)))
            throw new SecurityVaultException(msm.getString("invalidKeystorePasswordFormatMessage"));

        String salt = options.get(Constants.SALT);
        if (StringUtil.isEmpty(salt))
            throw new SecurityVaultException(msm.getString("invalidNullOrEmptyOptionMessage", Constants.SALT));

        String iterationCountStr = options.get(Constants.ITERATION_COUNT);
        if (StringUtil.isEmpty(iterationCountStr))
            throw new SecurityVaultException(msm.getString("invalidNullOrEmptyOptionMessage", Constants.ITERATION_COUNT));
        int iterationCount = Integer.parseInt(iterationCountStr);

        this.vaultAlias = options.get(Constants.VAULT_ALIAS);
        if (StringUtil.isEmpty(vaultAlias))
            throw new SecurityVaultException(msm.getString("invalidNullOrEmptyOptionMessage", Constants.VAULT_ALIAS));

        String keySizeStr = options.get(Constants.KEY_SIZE);
        if (!StringUtil.isEmpty(keySizeStr)) {
            keySize = Integer.parseInt(keySizeStr);
        }

        String encFileDir = options.get(Constants.ENC_FILE_DIR);
        if (StringUtil.isEmpty(encFileDir))
            throw new SecurityVaultException(msm.getString("invalidNullOrEmptyOptionMessage", Constants.ENC_FILE_DIR));

        keyStoreType = options.getOrDefault(Constants.KEYSTORE_TYPE, defaultKeyStoreType);

        pbe = new PBEUtils(salt, iterationCount);

        try {
            keyStorePWD = loadKeystorePassword(password);
            keystore = getKeyStore(keystoreURL);
        } catch (Exception e) {
            throw new SecurityVaultException(e);
        }

        // read and possibly convert vault content
        loadVault(encFileDir);

        log.info(sm.getString("picketBoxSecurityVault.vaultInitialized"));
        finishedInit = true;
    }

    /* (non-Javadoc)
     * @see org.jboss.security.vault.SecurityVault#isInitialized()
     */
    public boolean isInitialized() {
        return finishedInit;
    }

    /* (non-Javadoc)
     * @see org.jboss.security.vault.SecurityVault#keyList()
     */
    public Set<String> keyList() throws SecurityVaultException {
        return vaultContent.getVaultDataKeys();
    }

    @Override
    public String getLocation() {
        return Paths.get(vaultDir, VAULT_CONTENT_FILE).normalize().toString();
    }

    @Override
    public SecretKey getKey(String alias) {
        try {
            Entry e = keystore.getEntry(alias, new KeyStore.PasswordProtection(keyStorePWD));
            if (e instanceof KeyStore.SecretKeyEntry) {
                return ((KeyStore.SecretKeyEntry) e).getSecretKey();
            }
        } catch (Exception e) {
            log.info(sm.getString("picketBoxSecurityVault.vaultDoesNotContainSecretKey", alias));
            return null;
        }
        return null;
    }

    /**
     *
     * see org.jboss.security.vault.SecurityVault#store(java.lang.String, java.lang.String, char[], byte[])
     * @param vaultBlock     a string value that brings in the uniqueness
     * @param attributeName  name of the attribute
     * @param attributeValue
     * @throws SecurityVaultException
     */
    public void store(String vaultBlock, String attributeName, String attributeValue) throws SecurityVaultException {
        if (StringUtil.isEmpty(vaultBlock))
            throw new IllegalArgumentException(msm.getString("invalidNullArgument", "vaultBlock"));
        if (StringUtil.isEmpty(attributeName))
            throw new IllegalArgumentException(msm.getString("invalidNullArgument", "attributeName"));

        try {
            SecretKey keySpec = new SecretKeySpec(vaultKey.getEncoded(), Util.SECRET_KEY_TYPE);
            byte[] encryptedData = EncryptionUtil.encrypt(attributeValue.getBytes(Util.UTF8), keySpec);
            vaultContent.addVaultData(vaultAlias, vaultBlock, attributeName, encryptedData);
        } catch (Exception e1) {
            throw new SecurityVaultException(msm.getString("unableToEncryptDataMessage"), e1);
        }

        try {
            writeVaultData();
        } catch (IOException e) {
            throw new SecurityVaultException(msm.getString("unableToWriteVaultDataFileMessage", VAULT_CONTENT_FILE), e);
        }
    }

    /**
     *
     * see org.jboss.security.vault.SecurityVault#retrieve(java.lang.String, java.lang.String, byte[])
     * @param vaultBlock
     * @param attributeName
     * @return
     * @throws SecurityVaultException
     */
    public String retrieve(String vaultBlock, String attributeName) throws SecurityVaultException {
        if (StringUtil.isEmpty(vaultBlock))
            throw new IllegalArgumentException(msm.getString("invalidNullArgument", "vaultBlock"));
        if (StringUtil.isEmpty(attributeName))
            throw new IllegalArgumentException(msm.getString("invalidNullArgument", "attributeName"));

        byte[] encryptedValue = vaultContent.getVaultData(vaultAlias, vaultBlock, attributeName);

        if (encryptedValue == null) {
            throw new SecurityVaultException(sm.getString("picketBoxSecurityVault.attributeNotInVault", vaultAlias, vaultBlock, attributeName));
        }

        SecretKey secretKeySpec = new SecretKeySpec(vaultKey.getEncoded(), Util.SECRET_KEY_TYPE);
        try {
            return new String(EncryptionUtil.decrypt(encryptedValue, secretKeySpec), Util.UTF8);
        } catch (Exception e) {
            throw new SecurityVaultException(e);
        }
    }

    /* (non-Javadoc)
     * @see org.jboss.security.vault.SecurityVault#exists(java.lang.String, java.lang.String)
     */
    public boolean exists(String vaultBlock, String attributeName) {
        return vaultContent.containsData(vaultAlias, vaultBlock, attributeName);
    }

    /**
     *
     * see org.jboss.security.vault.SecurityVault#remove(java.lang.String, java.lang.String, byte[])
     * @param vaultBlock
     * @param attributeName
     * @return
     * @throws SecurityVaultException
     */
    public boolean remove(String vaultBlock, String attributeName)
            throws SecurityVaultException {
        if (StringUtil.isEmpty(vaultBlock))
            throw new IllegalArgumentException(msm.getString("invalidNullArgument", "vaultBlock"));
        if (StringUtil.isEmpty(attributeName))
            throw new IllegalArgumentException(msm.getString("invalidNullArgument", "attributeName"));

        try {
            if (vaultContent.deleteVaultData(vaultAlias, vaultBlock, attributeName)) {
                writeVaultData();
                return true;
            }
            return false;
        } catch (Exception e) {
            throw new SecurityVaultException(msm.getString("unableToWriteVaultDataFileMessage", VAULT_CONTENT_FILE), e);
        }
    }

    private char[] loadKeystorePassword(String passwordDef) throws Exception {
        final char[] password;

        if (StringUtil.startsWithIgnoreCase(passwordDef, SecurityVault.MASK_PROPERTY_PLACEHOLDER_PREFIX)) {
            String keystorePass = pbe.decode(passwordDef);
            password = keystorePass.toCharArray();
        } else {
            password = Util.loadPassword(passwordDef).toCharArray();
        }

        return password;
    }

    private void setUpVault() throws IOException, GeneralSecurityException {
        vaultContent = new SecurityVaultData();
        writeVaultData();

        vaultKey = getKey(vaultAlias);
        if (vaultKey == null) {
            throw new SecurityVaultException(msm.getString("vaultDoesnotContainSecretKey", vaultAlias));
        }
    }

    private void writeVaultData() throws IOException {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(getVaultFile());
            oos = new ObjectOutputStream(fos);
            oos.writeObject(vaultContent);
        } finally {
            Util.safeClose(oos);
            Util.safeClose(fos);
        }
    }

    private File getVaultFile() {
        return Paths.get(vaultDir).resolve(VAULT_CONTENT_FILE).toFile();
    }

    private boolean vaultFileExists() {
        return getVaultFile().canRead();
    }

    private boolean directoryExists(String dir) {
        File file = Paths.get(dir).toFile();
        return file.isDirectory();
    }

    private void loadVault(String encFileDir) throws SecurityVaultException {

        try {
            vaultDir = StringUtil.getSystemPropertyAsString(encFileDir);
            if (!directoryExists(vaultDir))
                throw new SecurityVaultException(msm.getString("fileOrDirectoryDoesNotExistMessage", vaultDir));

            if (vaultFileExists()) {
                readVault();
            } else {
                setUpVault();
            }

        } catch (Exception e) {
            throw new SecurityVaultException(e);
        }

    }

    private void readVault() throws IOException, ClassNotFoundException {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(getVaultFile());
            ois = new ObjectInputStream(fis);
            vaultContent = (SecurityVaultData) ois.readObject();
        } finally {
            Util.safeClose(fis);
            Util.safeClose(ois);
        }

        vaultKey = getKey(vaultAlias);
        if (vaultKey == null) {
            throw new RuntimeException(msm.getString("vaultDoesnotContainSecretKey", vaultAlias));
        }
    }

    /**
     * Get key store based on options passed to PicketBoxSecurityVault.
     *
     * @return
     */
    private KeyStore getKeyStore(String keystoreURL) {
        try {
            return KeyStoreUtil.getKeyStore(keyStoreType, keystoreURL, keyStorePWD);
        } catch (IOException | GeneralSecurityException e) {
            throw new RuntimeException(msm.getString("unableToGetKeyStore", keystoreURL), e);
        }
    }

}
