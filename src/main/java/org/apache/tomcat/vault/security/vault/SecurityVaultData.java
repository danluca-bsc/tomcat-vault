/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault.security.vault;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.res.StringManager;
import org.apache.tomcat.vault.util.Constants;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Security vault data store with version serialized data storage.
 *
 * @author Peter Skopek (pskopek_at_redhat_dot_com)
 * @author Dan Luca
 */
public class SecurityVaultData implements Serializable {

    private static final StringManager sm = StringManager.getManager(SecurityVaultData.class.getPackage().getName());
    private static final Log log = LogFactory.getLog(SecurityVaultData.class);
    private static final StringManager msm = StringManager.getManager("org.apache.tomcat.vault.security.resources");

    /** Do not change this suid, it is used for handling different versions of serialized data. */
    private static final long serialVersionUID = 1L;
    /** Version to denote actual version of SecurityVaultData object. */
    private static final int VERSION = 1;
    private transient Map<String, byte[]> vaultData = new ConcurrentHashMap<>();


    /**
     * Default constructor.
     */
    public SecurityVaultData() {
    }

    /**
     * Creates new format for data key in vault. All parameters has to be non-null.
     *
     * @param keyType      - currently not used (for possible future extension)
     * @param vaultBlock
     * @param attributeName
     * @return
     */
    private static String dataKey(String keyType, String vaultBlock, String attributeName) {
        return vaultBlock + Constants.PROPERTY_DEFAULT_SEPARATOR + attributeName;
    }

    /**
     * Creates a data key for the vault entry
     * @param vaultBlock
     * @param attributeName
     * @return
     */
    public static String dataKey(String vaultBlock, String attributeName) {
        return dataKey(null, vaultBlock, attributeName);
    }

    /**
     * Writes object to the ObjectOutputSteream.
     *
     * @param oos
     * @throws IOException
     */
    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.writeInt(VERSION);
        oos.writeObject(vaultData);
    }

    /**
     * Reads object from the ObjectInputStream. This method needs to be changed when implementing
     * changes in data and {@link #VERSION} is changed.
     *
     * @param ois
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        int version = ois.readInt();

        if (log.isDebugEnabled()) {
            log.debug(sm.getString("securityVaultData.securityVaultContentVersion", String.valueOf(version), String.valueOf(VERSION)));
        }

        if (version == VERSION) {
            this.vaultData = (Map<String, byte[]>) ois.readObject();
        } else {
            throw new RuntimeException(msm.getString("unrecognizedVaultContentVersion", String.valueOf(version), "1", String.valueOf(VERSION)));
        }
    }

    /**
     * Retrieves the data stored in vault storage.
     *
     * @param keyType      - currently not used (for possible future extension)
     * @param vaultBlock
     * @param attributeName
     * @return
     */
    byte[] getVaultData(String keyType, String vaultBlock, String attributeName) {
        return vaultData.get(dataKey(keyType, vaultBlock, attributeName));
    }

    /**
     *
     * @param keyType
     * @param vaultBlock
     * @param attributeName
     * @return
     */
    boolean containsData(String keyType, String vaultBlock, String attributeName) {
        return vaultData.containsKey(dataKey(keyType, vaultBlock, attributeName));
    }

    /**
     * @param keyType
     * @param vaultBlock
     * @param attributeName
     * @param encryptedData
     */
    void addVaultData(String keyType, String vaultBlock, String attributeName, byte[] encryptedData) {
        vaultData.put(dataKey(keyType, vaultBlock, attributeName), encryptedData);
    }

    /**
     * Removes data stored in vault storage.
     *
     * @param keyType
     * @param vaultBlock
     * @param attributeName
     * @return true when vault data has been removed successfully, otherwise false
     */
    boolean deleteVaultData(String keyType, String vaultBlock, String attributeName) {
        return vaultData.remove(dataKey(keyType, vaultBlock, attributeName)) != null;
    }

    /**
     * Returns mapping keys for all stored data.
     *
     * @return
     */
    Set<String> getVaultDataKeys() {
        return vaultData.keySet();
    }

}
