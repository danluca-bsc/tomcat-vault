/*
 * JBoss, Home of Professional Open Source
 * Copyright 2005, JBoss Inc., and individual contributors as indicated
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault.security;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.res.StringManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * External command password cache.
 * Singleton password cache.
 *
 * @author Peter Skopek (pskopek_at_redhat_dot_com)
 * @author Dan Luca
 */
public class ExternalPasswordCache implements PasswordCache {

    private static final StringManager sm = StringManager.getManager(ExternalPasswordCache.class.getPackage().getName());
    private static final Log log = LogFactory.getLog(ExternalPasswordCache.class);

    private static final ExternalPasswordCache PASSWORD_CACHE = new ExternalPasswordCache();

    private Map<String, PasswordRecord> cache;

    private ExternalPasswordCache() {
        cache = Collections.synchronizedMap(new HashMap<>());
    }

    public static ExternalPasswordCache getInstance() {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new RuntimePermission(ExternalPasswordCache.class.getName() + ".getExternalPasswordCacheInstance"));
        }
        return PASSWORD_CACHE;
    }

    /* (non-Javadoc)
     * @see org.jboss.security.PasswordCache#contains(java.lang.String)
     */
    @Override
    public boolean contains(String key) {
        String transformedKey = transformKey(key);
        PasswordRecord pr = cache.get(transformedKey);
        //for clarity - if we didn't find a password record, return false (note that the isValid call also checks for null inputs)
        if (pr == null) {
            return false;
        }
        boolean validPR = PasswordRecord.isValid(pr);
        if (!validPR) {
            cache.remove(transformedKey);
            log.trace(sm.getString("externalPasswordCache.expiredPasswordRemoved", key, transformedKey));
        }
        return validPR;
    }

    /* (non-Javadoc)
     * @see org.jboss.security.PasswordCache#getPassword(java.lang.String)
     */
    @Override
    public String getPassword(String key) {
        String newKey = transformKey(key);
        log.trace(sm.getString("externalPasswordCache.retrievingPasswordFromCache", key, newKey));
        PasswordRecord pr = cache.get(newKey);
        if (pr == null) {
            return null;
        }
        boolean validPR = PasswordRecord.isValid(pr);
        if (!validPR) {
            cache.remove(newKey);
            log.trace(sm.getString("externalPasswordCache.expiredPasswordRemoved", key, newKey));
            return null;
        }
        return pr.password;
    }

    /* (non-Javadoc)
     * @see org.jboss.security.PasswordCache#storePassword(java.lang.String, char[])
     */
    @Override
    public void storePassword(String key, String password, long timeout) {
        if (timeout < 0) {
            log.warn(sm.getString("externalPasswordCache.store.invalidTimeout", key, timeout));
            return;
        }
        String newKey = transformKey(key);
        log.trace(sm.getString("externalPasswordCache.storingPasswordToCache", key, newKey, timeout));
        PasswordRecord pr = new PasswordRecord();
        pr.timeOut = timeout > 0 ? System.currentTimeMillis() + timeout * 1000 : timeout;
        pr.password = password;
        cache.put(newKey, pr);
    }

    private String transformKey(String key) {
        String newKey = key;
        MessageDigest digest = getDigester();
        if (digest != null) {
            byte[] hash = digest.digest(key.getBytes(Util.UTF8));
            newKey = Base64.getEncoder().encodeToString(hash);
        }
        return newKey;
    }

    /**
     * Get number of cached passwords.
     * Mainly for testing purpose.
     */
    public int getCachedPasswordsCount() {
        return cache.size();
    }

    /* (non-Javadoc)
     * @see org.jboss.security.PasswordCache#reset()
     */
    @Override
    public void reset() {
        log.trace(sm.getString("externalPasswordCache.resettingCache"));
        cache.clear();
    }

    static MessageDigest getDigester() {
        try {
            return MessageDigest.getInstance(Util.DIGEST_ALG);
        } catch (NoSuchAlgorithmException e) {
            // Cannot get algorithm instance for hashing password commands. Using NULL.
            log.error(sm.getString("externalPasswordCache.cannotGetAlgorithmInstance"));
        }
        return null;
    }
}

class PasswordRecord {

    long timeOut;
    String password;

    static boolean isValid(PasswordRecord pr) {
        if (pr == null) {
            return false;
        }
        return (pr.timeOut == 0) || (pr.timeOut <= System.currentTimeMillis());
    }

}
