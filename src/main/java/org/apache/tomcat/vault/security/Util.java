/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2006, Red Hat Middleware LLC, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault.security;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.res.StringManager;
import org.apache.tomcat.vault.util.StringUtil;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Util.
 *
 * @author Scott.Stark@jboss.org
 * @author <a href="adrian@jboss.com">Adrian Brock</a>
 */
public abstract class Util {
    public static final Charset UTF8 = StandardCharsets.UTF_8;
    public static final String DIGEST_ALG = "SHA-256";
    public static final String SECRET_KEY_TYPE = "AES";
    public static final int DEFAULT_KEY_SIZE = 256;

    private static final StringManager strm = StringManager.getManager(Util.class.getPackage().getName());
    private static final Log log = LogFactory.getLog(Util.class);
    private static final StringManager msm = StringManager.getManager("org.apache.tomcat.vault.security.resources");
    /** Regex for matching password commands - group 1 (optional) is expiration time in millis, group 2 is the actual command  */
    private static final Pattern reCmd = Pattern.compile("#cmd(:-?\\d+)?\\{(.+)\\}", Pattern.CASE_INSENSITIVE);

    private static PasswordCache externalPasswordCache;

    /**
     * Execute a password load command to obtain the string contents of a password.<br/>
     * <p>
     *     <b>Note:</b> I fail to see the usefulness of this type of password specification, as password needed to operate the vault are fixed and protections in place for the
     *     vault properties or masked passwords are equally efficient when compared to executing commands that render the same value. Definitely caching the passwords for a
     *     finite amount of time doesn't make sense.
     * </p>
     *
     * @param passwordCmd - A command to execute to obtain the plaintext password. The format is <code>#cmd[:expiration_in_seconds]{...}</code>.
     *                    The general command '...' is a string delimited by ' ' (space) where the first part is the actual command
     *                    and further parts represents its parameters. The space can be backslashed in order to keep it as a part of the parameter.<br/>
     *                    The <code>expiration_in_seconds</code>, if specified, determines the amount of time in seconds to cache the password value.
     *                    Default cache expiration is 0 = infinity. To disable caching, specify <code>-1</code> as the value.
     * @return the password characters
     * @throws Exception
     */
    public static String loadPassword(String passwordCmd) throws Exception {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new RuntimePermission(Util.class.getName() + ".loadPassword"));
        }
        String password = null;
        String command = null;
        long cacheTimeout = 0;

        // Look for a #cmd prefix indicating a password command
        if (StringUtil.startsWithIgnoreCase(passwordCmd, "#cmd")) {
            Matcher m = reCmd.matcher(passwordCmd);
            if (m.matches()) {
                try {
                    if (!StringUtil.isEmpty(m.group(1))) {
                        cacheTimeout = Long.parseLong(m.group(1));
                    }
                } catch (Exception e) {
                    // ignore
                    log.error(strm.getString("util.parsingTimeoutNumber", m.group(1), e.getClass().getSimpleName(), e.getMessage()));
                }
                command = m.group(2);
            } else {
                throw new IllegalArgumentException(msm.getString("invalidPasswordCommandType", passwordCmd));
            }
        } else {
            // Its just the password string
            password = passwordCmd;
        }

        if (StringUtil.isEmpty(password)) {
            // Load the password
            if (externalPasswordCache == null) {
                externalPasswordCache = ExternalPasswordCache.getInstance();
            }
            if (externalPasswordCache.contains(passwordCmd)) {
                password = externalPasswordCache.getPassword(passwordCmd);
            } else {
                password = execCmd(command);
                if (password != null) {
                    externalPasswordCache.storePassword(passwordCmd, password, cacheTimeout);
                }
            }
        }
        return password;
    }

    private static String execCmd(String cmd) throws Exception {
        log.trace(strm.getString("util.beginExecPasswordCmd", cmd));
        SecurityManager sm = System.getSecurityManager();
        String line;
        if (sm != null) {
            line = RuntimeActions.PRIVILEGED.execCmd(cmd);
        } else {
            line = RuntimeActions.NON_PRIVILEGED.execCmd(cmd);
        }
        return line;
    }

    interface RuntimeActions {
        RuntimeActions PRIVILEGED = new RuntimeActions() {
            public String execCmd(final String cmd) throws Exception {
                try {
                    return AccessController.doPrivileged((PrivilegedExceptionAction<String>) () -> NON_PRIVILEGED.execCmd(cmd));
                } catch (PrivilegedActionException e) {
                    throw e.getException();
                }
            }
        };
        RuntimeActions NON_PRIVILEGED = new RuntimeActions() {
            public String execCmd(final String cmd) throws Exception {
                final String[] parsedCommand = parseCommand(cmd);
                final ProcessBuilder builder = new ProcessBuilder(parsedCommand);
                final Process process = builder.start();
                final String line;
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                    line = reader.readLine();
                }
                int exitCode = process.waitFor();
                log.trace(strm.getString("util.endExecPasswordCmd", exitCode));
                return line;
            }
            protected String[] parseCommand(String command) {
                // space can be backslashed
                final String[] parsedCommand = command.split("(?<!\\\\) ");
                for (int k = 0; k < parsedCommand.length; k++) {
                    if (parsedCommand[k].indexOf('\\') != -1)
                        parsedCommand[k] = parsedCommand[k].replaceAll("\\\\ ", " ");
                }
                return parsedCommand;
            }
        };

        String execCmd(String cmd) throws Exception;
    }

    /**
     * Checks whether password can be loaded by {@link #loadPassword(String)}.
     *
     * @param passwordCmd a potential password command
     * @return true if password can be loaded by {@link #loadPassword(String)}, false otherwise.
     */
    public static boolean isPasswordCommand(String passwordCmd) {
        return !StringUtil.isEmpty(passwordCmd) && (passwordCmd.startsWith("#cmd"));
    }

    public static void safeClose(InputStream fis) {
        safeClose(fis, null);
    }

    public static void safeClose(InputStream fis, String logMsg) {
        try {
            if (fis != null) {
                fis.close();
            }
        } catch (Exception e) {
            if (!StringUtil.isEmpty(logMsg)) {
                log.error(String.format("%s - %s: %s", logMsg, e.getClass().getSimpleName(), e.getMessage()));
            }
        }
    }

    public static void safeClose(OutputStream os) {
        safeClose(os, null);
    }

    public static void safeClose(OutputStream os, String logMsg) {
        try {
            if (os != null) {
                os.close();
            }
        } catch (Exception e) {
            if (!StringUtil.isEmpty(logMsg)) {
                log.error(String.format("%s - %s: %s", logMsg, e.getClass().getSimpleName(), e.getMessage()));
            }
        }
    }

}
