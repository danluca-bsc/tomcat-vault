/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2008, Red Hat Middleware LLC, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors. 
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault.security.vault;

import org.apache.tomcat.vault.util.Constants;

import javax.crypto.SecretKey;
import java.security.Key;
import java.util.Map;
import java.util.Set;

/**
 * Vault for secure storage of attributes
 *
 * @author Anil.Saldhana@redhat.com
 * @author Dan Luca
 */
public interface SecurityVault {

    String VAULT_PROPERTY_PLACEHOLDER_PREFIX = "#vault[";
    String RE_VAULT_PROPERTY_PLACEHOLDER_PREFIX = VAULT_PROPERTY_PLACEHOLDER_PREFIX.replace("[", "\\[");
    String MASK_PROPERTY_PLACEHOLDER_PREFIX = "#mask[";
    String RE_MASK_PROPERTY_PLACEHOLDER_PREFIX = MASK_PROPERTY_PLACEHOLDER_PREFIX.replace("[", "\\[");
    String DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX = "]";
    String RE_DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX = "\\]";
    String VAULT_SECTION_SEPARATOR = Constants.PROPERTY_DEFAULT_SEPARATOR;

    /**
     * Retrieves the location of the vault file
     * @return
     * @since 1.2.0
     */
    String getLocation();

    /**
     * Initialize the vault
     *
     * @param options
     */
    void init(Map<String, String> options) throws SecurityVaultException;

    /**
     * Determine if the vault is initialized
     *
     * @return
     */
    boolean isInitialized();

    /**
     * Get the currently vaulted VaultBlock_attribute Names
     *
     * @return
     * @throws SecurityVaultException
     */
    Set<String> keyList() throws SecurityVaultException;

    /**
     * Check whether an attribute value exists in the vault
     *
     * @param vaultBlock
     * @param attributeName
     * @return
     * @throws SecurityVaultException
     */
    boolean exists(String vaultBlock, String attributeName) throws SecurityVaultException;

    /**
     * Store an attribute value
     *
     * @param vaultBlock     a string value that brings in the uniqueness
     * @param attributeName  name of the attribute
     * @param attributeValue
     * @throws SecurityVaultException
     */
    void store(String vaultBlock, String attributeName, String attributeValue) throws SecurityVaultException;

    /**
     * Retrieve the attribute value
     *
     * @param vaultBlock
     * @param attributeName
     * @return
     * @throws SecurityVaultException
     */
    String retrieve(String vaultBlock, String attributeName) throws SecurityVaultException;

    /**
     * Remove an existing attribute value
     *
     * @param vaultBlock
     * @param attributeName
     * @return true if remove was successful, false otherwise
     * @throws SecurityVaultException
     */
    boolean remove(String vaultBlock, String attributeName) throws SecurityVaultException;

    /**
     * Retrieves a (symmetric) key by its alias, if it exists
     * @param alias key alias to retrieve
     * @return the Secret Key with given alias, if exists, null otherwise
     * @since 1.2.0
     */
    SecretKey getKey(String alias);
}