/*
 * JBoss, Home of Professional Open Source
 * Copyright 2005, JBoss Inc., and individual contributors as indicated
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.apache.tomcat.vault.security.plugins;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.res.StringManager;
import org.apache.tomcat.vault.security.Util;
import org.apache.tomcat.vault.security.vault.PicketBoxSecurityVault;
import org.apache.tomcat.vault.security.vault.SecurityVault;
import org.apache.tomcat.vault.util.Constants;
import org.apache.tomcat.vault.util.EncryptionUtil;
import org.apache.tomcat.vault.util.StringUtil;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Encrypt a password using the key generated from a pre-defined password
 *
 * @author Dan Luca
 */
public class PBEUtils {
    private static final StringManager sm = StringManager.getManager(PBEUtils.class.getPackage().getName());
    private static final Log log = LogFactory.getLog(PBEUtils.class);
    private static final StringManager msm = StringManager.getManager("org.apache.tomcat.vault.security.resources");

    private static final String PBE_KEY_ALG = "PBKDF2WithHmacSHA256";
    private static final int KEY_SIZE = Util.DEFAULT_KEY_SIZE;
    private static final int mask_prefix_length = SecurityVault.MASK_PROPERTY_PLACEHOLDER_PREFIX.length();
    private static final int mask_suffix_length = SecurityVault.DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX.length();

    private final String salt;
    private final int iterationCount;

    public PBEUtils(String saltValue, int iterCount) {
        this.salt = saltValue;
        this.iterationCount = iterCount;
    }

    public String decode(String maskedString) throws GeneralSecurityException, IOException {
        if (StringUtil.startsWithIgnoreCase(maskedString, SecurityVault.MASK_PROPERTY_PLACEHOLDER_PREFIX) && maskedString.endsWith(SecurityVault.DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX)) {
            SecretKey cipherKey = PBEUtils.generateKey(StringUtil.mix("somearbitrarycrazystringthatdoesnotmatter",
                "PgZ(3U)r0Nx8qh7}Va\\]ImJ%$CE6uD9W^/F{?SdL", "b?9J+Z@>N|rp"), salt, iterationCount);
            String maskedValue = maskedString.substring(mask_prefix_length, maskedString.length() - mask_suffix_length);
            maskedString = EncryptionUtil.decryptB64(maskedValue, cipherKey);
        }
        return maskedString;
    }

    public String encode(String clearString) throws GeneralSecurityException, IOException {
        SecretKey cipherKey = PBEUtils.generateKey(StringUtil.mix("somearbitrarycrazystringthatdoesnotmatter",
            "PgZ(3U)r0Nx8qh7}Va\\]ImJ%$CE6uD9W^/F{?SdL", "b?9J+Z@>N|rp"), salt, iterationCount);
        return SecurityVault.MASK_PROPERTY_PLACEHOLDER_PREFIX + EncryptionUtil.encryptB64(clearString, cipherKey) + SecurityVault.DEFAULT_PROPERTY_PLACEHOLDER_SUFFIX;
    }

    public String getSalt() {
        return salt;
    }

    public int getIterationCount() {
        return iterationCount;
    }

    public static SecretKey generateKey(String password, String salt, int iterCount) throws GeneralSecurityException {
        PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt.getBytes(Util.UTF8), iterCount, KEY_SIZE);
        SecretKeyFactory factory = SecretKeyFactory.getInstance(PBE_KEY_ALG);
        SecretKey pbeKey = factory.generateSecret(keySpec);
        return new SecretKeySpec(pbeKey.getEncoded(), Util.SECRET_KEY_TYPE);
    }
}
