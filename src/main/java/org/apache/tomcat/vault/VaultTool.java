/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2011, Red Hat, Inc., and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.apache.tomcat.vault;

import org.apache.commons.cli.*;
import org.apache.tomcat.vault.security.plugins.PBEUtils;
import org.apache.tomcat.vault.security.vault.SecurityVault;
import org.apache.tomcat.vault.security.vault.SecurityVaultData;
import org.apache.tomcat.vault.util.Constants;
import org.apache.tomcat.vault.util.PropertyFileManager;
import org.apache.tomcat.vault.util.StringUtil;

import java.io.Console;
import java.io.File;
import java.io.PrintStream;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.InputMismatchException;
import java.util.Properties;
import java.util.Scanner;

/**
 * Command Line Tool for the default implementation of the {@link SecurityVault}
 *
 * @author Anil Saldhana
 * @author Peter Skopek
 * @author Dan Luca
 */
public class VaultTool {

    public static final String KEYSTORE_PARAM = "keystore";
    public static final String KEYSTORE_PASSWORD_PARAM = "keystore-password";
    public static final String KEYSTORE_TYPE = "keystore-type";
    public static final String ENC_DIR_PARAM = "enc-dir";
    public static final String SALT_PARAM = "salt";
    public static final String ITERATION_PARAM = "iteration";
    public static final String VAULT_ALIAS_PARAM = "alias-vault";
    public static final String INLINE_ALIAS_PARAM = "alias-inline";
    public static final String VAULT_BLOCK_PARAM = "vault-block";
    public static final String ATTRIBUTE_PARAM = "attribute";
    public static final String SEC_ATTR_VALUE_PARAM = "sec-attr";
    public static final String CHECK_SEC_ATTR_EXISTS_PARAM = "check-sec-attr";
    public static final String LIST_VAULT_ENTRIES_PARAM = "list-vault";
    public static final String REMOVE_SEC_ATTR = "remove-sec-attr";
    public static final String GENERATE_CONFIG_FILE = "generate-config";
    public static final String HELP_PARAM = "help";
    public static final String INLINE = "encrypt";
    public static final String MASK = "mask";
    public static final String FILE_CONFIG_PARAM = "file-config";

    private static boolean skipSummary = false;

    private VaultInteractiveSession session = null;
    private VaultSession nonInteractiveSession = null;

    private Options options = null;
    private CommandLine cmdLine = null;

    public void setSession(VaultInteractiveSession sess) {
        session = sess;
    }

    public VaultInteractiveSession getSession() {
        return session;
    }

    public static void main(String[] args) {

        VaultTool tool = null;

        if (args != null && args.length > 0) {
            int returnVal = 0;
            try {
                tool = new VaultTool(args);
                returnVal = tool.execute();
                if (!skipSummary) {
                    tool.summary();
                }
            } catch (Exception e) {
                System.err.println("Problem occurred:");
                System.err.println(String.format("%s: %s", e.getClass().getName(), e.getMessage()));
                System.exit(1);
            }
            System.exit(returnVal);
        } else {
            tool = new VaultTool();

            Console console = System.console();

            if (console == null) {
                System.err.println("No console.");
                System.exit(1);
            }

            Scanner in = new Scanner(System.in);
            while (true) {
                String commandStr = "Please enter a Digit::%n  0: Start Interactive Session%n"
                        + "  1: Remove Interactive Session%n  Other: Exit%n";

                System.out.printf(commandStr);
                int choice = -1;

                try {
                    choice = in.nextInt();
                } catch (InputMismatchException e) {
                    System.err.println("'" + in.next() + "' is not a digit. Restart and enter a digit.");
                    System.exit(3);
                }

                switch (choice) {
                    case 0:
                        System.out.println("Starting an interactive session");
                        VaultInteractiveSession vsession = new VaultInteractiveSession();
                        tool.setSession(vsession);
                        vsession.start();
                        break;
                    case 1:
                        System.out.println("Removing the current interactive session");
                        tool.setSession(null);
                        break;
                    default:
                        System.exit(0);
                }
            }

        }

    }

    public VaultTool(String[] args) {
        initOptions();
        CommandLineParser parser = new DefaultParser();
        try {
            cmdLine = parser.parse(options, args, true);
        } catch (MissingArgumentException e) {
            Option opt = e.getOption();
            for (String s : args) {
                String optionSpecified = s.replaceAll("^-+", "");
                if (optionSpecified.equals(opt.getLongOpt()) ||
                    optionSpecified.equals(opt.getOpt())) {
                    System.err.println("Missing argument for option: " + optionSpecified);
                    System.exit(2);
                }
            }
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            System.exit(2);
        }
    }

    public VaultTool() {
    }

    /**
     * Build options for non-interactive VaultTool usage scenario.
     *
     * @return
     */
    private void initOptions() {
        options = new Options();
        options.addOption("k", KEYSTORE_PARAM, true, "Keystore URL. Defaults to './conf/vault/vault.p12'");
        options.addOption("p", KEYSTORE_PASSWORD_PARAM, true, "Keystore password");
        options.addOption("t", KEYSTORE_TYPE, true, "Keystore type (JCEKS or PKCS12). Defaults to PKCS12");
        options.addOption("e", ENC_DIR_PARAM, true, "Directory containing encrypted files. Defaults to './conf/vault'");
        options.addOption("s", SALT_PARAM, true, "Random salt");
        options.addOption("i", ITERATION_PARAM, true, "Iteration count. Defaults to 187");
        options.addOption("Av", VAULT_ALIAS_PARAM, true, "Key alias in keystore for vault stored attributes. Defaults to 'vault'");
        options.addOption("Ai", INLINE_ALIAS_PARAM, true, "Key alias in keystore for inline encrypted attributes. Defaults to 'inline'");
        options.addOption("b", VAULT_BLOCK_PARAM, true, "Vault block");
        options.addOption("a", ATTRIBUTE_PARAM, true, "Attribute name");
        options.addOption("f", FILE_CONFIG_PARAM, true, "File configuration - ignores keystore url, password, type, encoding directory, salt, iteration count, alias parameters");

        OptionGroup og = new OptionGroup();
        Option x = new Option("x", SEC_ATTR_VALUE_PARAM, false,
            "Secure attribute value (such as password) to store. If no argument is specified, the password value is read from the console");
        Option c = new Option("c", CHECK_SEC_ATTR_EXISTS_PARAM, false, "Check whether the attribute name already exists in the vault");
        Option r = new Option("r", REMOVE_SEC_ATTR, false, "Remove the attribute from the vault");
        Option g = new Option("g", GENERATE_CONFIG_FILE, true, "Path for generated config file");
        Option h = new Option("h", HELP_PARAM, false, "Help");
        Option l = new Option("l", LIST_VAULT_ENTRIES_PARAM, false, "List Vault entry names");
        Option E = new Option("E", INLINE, false, "Encrypt value using inline feature. If no argument is specified, the password value is read from the console");
        Option M = new Option("M", MASK, false, "Encrypt value using masking feature. If no argument is specified, the password value is read from the console");
        og.addOption(x);
        og.addOption(c);
        og.addOption(r);
        og.addOption(g);
        og.addOption(h);
        og.addOption(E);
        og.addOption(l);
        og.addOption(M);
        og.setRequired(true);
        options.addOptionGroup(og);
    }

    private int execute() throws Exception {

        if (cmdLine.hasOption(HELP_PARAM)) {
            // Just print the usage. Printing summary is not required here.
            skipSummary = true;
            printUsage();
            return 100;
        }

        URI thisJar = VaultTool.class.getProtectionDomain().getCodeSource().getLocation().toURI();
        Path jarFolder = Paths.get(thisJar).getParent();
        String keystorePassword, keystoreURL, keystoreType, keyAlias, inlineAlias, salt, encryptionDirectory;
        int iterationCount = -1;
        if (cmdLine.hasOption(FILE_CONFIG_PARAM)) {
            Path cfgFilePath = Paths.get(cmdLine.getOptionValue(FILE_CONFIG_PARAM)).toAbsolutePath().normalize();
            PropertyFileManager pfmCfg = new PropertyFileManager(cfgFilePath.toString());
            Properties cfg = pfmCfg.load();
            salt = cfg.getProperty(Constants.SALT);
            iterationCount = Integer.parseInt(cfg.getProperty(Constants.ITERATION_COUNT));
            keystorePassword = new PBEUtils(salt, iterationCount).decode(cfg.getProperty(Constants.KEYSTORE_PASSWORD));
            keyAlias = cfg.getProperty(Constants.VAULT_ALIAS);
            inlineAlias = cfg.getProperty(Constants.INLINE_ALIAS);
            keystoreURL = cfg.getProperty(Constants.KEYSTORE_URL);
            keystoreType = cfg.getProperty(Constants.KEYSTORE_TYPE);
            encryptionDirectory = cfg.getProperty(Constants.ENC_FILE_DIR);
            //if provided a file config, no need to keep printing summary config
            skipSummary = true;

        } else {
            salt = cmdLine.getOptionValue(SALT_PARAM, "Z3YEhg86f7k1+c\\)=AWP");
            iterationCount = Integer.parseInt(cmdLine.getOptionValue(ITERATION_PARAM, "187"));

            keystorePassword = cmdLine.getOptionValue(KEYSTORE_PASSWORD_PARAM);
            keystoreType = cmdLine.getOptionValue(KEYSTORE_TYPE, "PKCS12");

            String encDirDefault = jarFolder.resolve("../conf/vault").normalize().toString();
            encryptionDirectory = cmdLine.getOptionValue(ENC_DIR_PARAM, encDirDefault);

            String keystorePathDefault = jarFolder.resolve("../conf/vault/vault.p12").normalize().toString();
            keystoreURL = cmdLine.getOptionValue(KEYSTORE_PARAM, keystorePathDefault);
            keyAlias = cmdLine.getOptionValue(VAULT_ALIAS_PARAM, "vault");
            inlineAlias = cmdLine.getOptionValue(INLINE_ALIAS_PARAM, "inline");
        }

        //encrypting masked values do not require keystore, but they do require salt and iteration count
        if (cmdLine.hasOption(MASK)) {
            PBEUtils pbe = new PBEUtils(salt, iterationCount);
            switch (cmdLine.getArgs().length) {
                case 1:
                    System.out.printf("Encrypted masked value: %s%n", pbe.encode(cmdLine.getArgs()[0]));
                    return 0;
                case 0:
                    String psw = VaultInteractiveSession.getSensitiveValue("Enter password to encrypt (mask) ");
                    System.out.printf("Encrypted masked value: %s%n", pbe.encode(psw));
                    return 0;
                default:
                    System.out.println("Arguments accepted: password to encrypt or none (read from console)");
                    return 100;
            }
        }

        //ensure the keystore password is specified
        if (StringUtil.isEmpty(keystorePassword)) {
            keystorePassword = VaultInteractiveSession.getSensitiveValue("Enter keystore password ");
        }

        nonInteractiveSession = new VaultSession(keystoreURL, keystorePassword, keystoreType, encryptionDirectory, salt, iterationCount);
        nonInteractiveSession.startVaultSession(keyAlias, inlineAlias);

        String vaultBlock = cmdLine.getOptionValue(VAULT_BLOCK_PARAM);
        String attributeName = cmdLine.getOptionValue(ATTRIBUTE_PARAM);

        if (cmdLine.hasOption(CHECK_SEC_ATTR_EXISTS_PARAM)) {
            if (!(cmdLine.hasOption(VAULT_BLOCK_PARAM) && cmdLine.hasOption(ATTRIBUTE_PARAM))) {
                System.out.println("Both Vault block and Attribute name are required.");
                return 100;
            }
            // check password
            if (nonInteractiveSession.checkSecuredAttribute(vaultBlock, attributeName)) {
                System.out.println("The vault already contains a value for entry: " + SecurityVaultData.dataKey(vaultBlock, attributeName));
                return 0;
            } else {
                System.out.println("The vault does NOT have an entry for: " + SecurityVaultData.dataKey(vaultBlock, attributeName));
                return 5;
            }
        } else if (cmdLine.hasOption(SEC_ATTR_VALUE_PARAM)) {
            if (!(cmdLine.hasOption(VAULT_BLOCK_PARAM) && cmdLine.hasOption(ATTRIBUTE_PARAM))) {
                System.out.println("Both Vault block and Attribute name are required.");
                return 100;
            }
            switch (cmdLine.getArgs().length) {
                case 1:
                    // add password from CLI argument
                    nonInteractiveSession.addSecuredAttribute(vaultBlock, attributeName, cmdLine.getArgs()[0]);
                    break;
                case 0:
                    // read and add password
                    String password = VaultInteractiveSession.getSensitiveValue(String.format("Enter attribute %s::%s value ", vaultBlock, attributeName));
                    nonInteractiveSession.addSecuredAttribute(vaultBlock, attributeName, password);
                    break;
                default:
                    System.out.println("Too many arguments - only 1 (the password to secure) or none (read password from console) are accepted for option " + SEC_ATTR_VALUE_PARAM);
                    return 100;
            }
            return 0;
        } else if (cmdLine.hasOption(REMOVE_SEC_ATTR)) {
            if (!(cmdLine.hasOption(VAULT_BLOCK_PARAM) && cmdLine.hasOption(ATTRIBUTE_PARAM))) {
                System.out.println("Both Vault block and Attribute name are required.");
                return 100;
            }
            nonInteractiveSession.removeSecuredAttribute(vaultBlock, attributeName);
            return 0;
        } else if (cmdLine.hasOption(LIST_VAULT_ENTRIES_PARAM)) {
            nonInteractiveSession.listVaultKeys();
            return 0;
        } else if (cmdLine.hasOption(GENERATE_CONFIG_FILE)) {
            String configFile = cmdLine.getOptionValue(GENERATE_CONFIG_FILE);
            //if specified as a file name only, then it is relative to ../conf - to match a tomcat installation folder structure
            Path pathCfg = Paths.get(configFile);
            if ((pathCfg.getParent() == null) && !pathCfg.isAbsolute()) {
                configFile = jarFolder.resolve("../conf").resolve(pathCfg).normalize().toString();
                File cfgDir = Paths.get(configFile).getParent().toFile();
                if (!cfgDir.isDirectory()) {
                    cfgDir.mkdirs();
                }
            }
            try(PrintStream ps = new PrintStream(configFile)) {
                nonInteractiveSession.outputConfig(ps);
            }
            return 0;
        } else if (cmdLine.hasOption(INLINE)) {
            // Regardless of the return here, we do not need to print summary for this command option
            skipSummary = true;
            // We need the value to encrypt - either provided as argument, or read from console
            switch (cmdLine.getArgs().length) {
                case 1:
                    nonInteractiveSession.encryptInlineValue(cmdLine.getArgs()[0]);
                    return 0;
                case 0:
                    String psw = VaultInteractiveSession.getSensitiveValue("Enter password to encrypt (inline) ");
                    nonInteractiveSession.encryptInlineValue(psw);
                    return 0;
                default:
                    System.out.println("Arguments: password to encrypt or none (read from console)");
                    return 100;
            }
        }

        // Printing summary is not required here
        skipSummary = true;
        return 100;
    }

    private void summary() {
        nonInteractiveSession.vaultConfigurationDisplay();
    }

    private void printUsage() {
        HelpFormatter help = new HelpFormatter();
        String suffix = (VaultTool.isWindows() ? ".bat" : ".sh");
        help.printHelp("vault" + suffix + " <empty> | ", options, true);
    }

    public static boolean isWindows() {
        String opsys = System.getProperty("os.name").toLowerCase();
        return (opsys.contains("win"));
    }
}
