# Vault for Apache Tomcat
Tomcat-vault is a PicketLink vault extension for Apache Tomcat. It allows you to place sensitive information, such as passwords, inside of a vault instead of the Tomcat configuration files.

## Build status

[![pipeline status](https://gitlab.com/danluca/tomcat-vault/badges/master/pipeline.svg)](https://gitlab.com/danluca/tomcat-vault/-/commits/master)

### Latest test results
* [Tomcat 8.5](https://gitlab.com/danluca/tomcat-vault/-/jobs/artifacts/master/file/build/reports/tests/test/index.html?job=test85)
* [Tomcat 9.0](https://gitlab.com/danluca/tomcat-vault/-/jobs/artifacts/master/file/build/reports/tests/test/index.html?job=test90)

# Installation
See the [INSTALL](./INSTALL.md) file for instructions on installation and usage.

Latest release - [1.2.0 RC1](https://gitlab.com/danluca/tomcat-vault/-/releases/1.2.0-rc1)

# How it works
At start up, the Tomcat digester module parses configuration files and resolves all `${parameter}` placeholders found within Tomcat XML configuration files. 
If the `parameter` indicates a vault stored value - e.g. `#vault[block:attribute]`, then the `${#vault[block:attribute]}` is replaced with the **decrypted** value of the corresponding vault stored attribute.

# Links
Tomcat System Properties :

https://tomcat.apache.org/tomcat-9.0-doc/config/systemprops.html

https://tomcat.apache.org/tomcat-8.5-doc/config/systemprops.html
